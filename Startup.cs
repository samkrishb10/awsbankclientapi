using AWSBankClientAPI.Models;
using AWSBankClientAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using System.Text;

namespace AWSBankClientAPI
{
    public class Startup
    {
        //private readonly string allowAllOrigins = "_AllowAllOrigins";
        //private readonly string restrictOrigins = "_RestrictOrigins";
        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        //private readonly AWSBANKAPIContext _context;
        private IWebHostEnvironment HostingEnvironment { get; set; }
        //public AuthenticateService(IOptions<AppSettings> appSettings, AWSBANKAPIContext context)
        //{
        //    _appSettings = appSettings.Value;
        //    _context = context;
        //}
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //    services.AddCors(options =>
            //    options.AddPolicy("AllowOrigin", options => options.WithOrigins("http://litefin-website.s3-website.ap-south-1.amazonaws.com", "http://localhost:4200", "https://cqbk8uay5f.execute-api.ap-south-1.amazonaws.com")
            //     .AllowAnyHeader()
            //.AllowAnyMethod().AllowCredentials()));
            services.AddCors();
            services.AddSwaggerDocumentation();
            //options.AddDefaultPolicy(
            //        builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));
            var connectionString = Configuration.GetConnectionString("AWSPSQLDatabase");
            services.AddDbContext<psqlbankapiContext>(opt => opt.UseNpgsql(connectionString));
            services.AddControllers();

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingsSection);
            //JWT Authentication
            var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(appSettings.Key);
            services.AddAuthentication(au =>
            {
                au.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                au.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(jwt =>
            {
                jwt.RequireHttpsMetadata = false;
                jwt.SaveToken = true;
                jwt.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddScoped<IAuthenticateService, AuthenticateService>();
            services.AddScoped<IBankMethodsService, BankMethodsService>();
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo
            //    {
            //        Version = "v1",
            //        Title = "AWSBankClientAPI",
            //        Description = "Testing"
            //    });
            //    c.AddSecurityDefinition("Bearer", new ApiKeyScheme
            //    {
            //        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
            //        Name = "Authorization",
            //        In = "header",
            //        Type = "apiKey"
            //    });

            //c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
            //{
            //    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
            //    Name = "Authorization",
            //    In = "header",
            //    Type = "apiKey"
            //});

            //c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
            //{
            //    { "Bearer", new string[] { } }
            //});

            //});
         
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();

            app.UseRouting();
            //app.UseCors("AllowOrigin");
            app.UseCors(x => x
              .AllowAnyMethod()
              .AllowAnyHeader()
              .SetIsOriginAllowed(origin => true) // allow any origin
              .AllowCredentials()); // allow credentials
            //app.UseCors(options =>
            //  options.AllowAnyHeader();
            //options.AllowAnyMethod();
            //options.WithOrigins("AllowOrigin");
            //));
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwaggerDocumentation();

            //app.UseSwagger();

            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "AWSBankClientAPI");
            //});
        }
    }
}
