﻿using AWSBankClientAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSBankClientAPI.Services
{
    public interface IBankMethodsService
    {
        string Getsummary(string email, int banktypeid);
        string GetAccountListsummary(string email);
        bool UpdateAccountSummery(string email);
        bool UpdateBankAccountSummery(int customerid, string AccountNo, int BankId);
        string GetTransactions(string email, string from_date, string to_date);
        string GetSpecTransactions(string email, int banktypeid, string from_date, string to_date);
        string GetAllTransactions(string email, string from_date, string to_date, int bankid);
        string GetTransactionsbycategory(string email, string from_date, string to_date, int bankid);
        Task<int> AddBank(BankAccount bankaccount);
        int DeleteBank(int customerId, int bankId);
        Task<List<Secureddata>> getBankDetails(BankAccount bankaccount);
        int InitTransactiondata(int customerid, int bankid);
        string Getupdatedsummery(int customerid);
        int Updateotp(int CustomerId, int BankId, string Username, int otp);
    }
}
