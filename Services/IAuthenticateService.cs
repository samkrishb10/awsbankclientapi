﻿using AWSBankClientAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSBankClientAPI.Services
{
    public interface IAuthenticateService
    {
        CustomerProfile Authenticate(string email);
        int MailValidation(string email);
        CustomerProfile VerifyOTPEmail(string email, int? otp);
        CustomerProfile UpdateInfo(CustomerProfile info);
        //string GenerateJWT(string userName, string apikey, string secretkey);
        UserAccount GenerateJWT(string userName, string apikey);
    }
}
