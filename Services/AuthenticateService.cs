﻿using AWSBankClientAPI.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Mail;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;

namespace AWSBankClientAPI.Services
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly AppSettings _appSettings;
        private readonly psqlbankapiContext _context;
        public AuthenticateService(IOptions<AppSettings> appSettings, psqlbankapiContext context)
        {
            _appSettings = appSettings.Value;
            _context = context;
        }
        //private List<User> users = new List<User>()
        //{
        //    new User{ UserId=1,FirstName="Samy",LastName="Arockia",UserName="sam",Password="Welcome1!"}
        //};
        public int MailValidation(string email)
        {
            bool bSent = false;
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email && x.Customerid == x.Managementid && x.Customerid > 0);
            if (user == null)
                return -1;
            string otp = GenerateOTP();
            SendPasswordEmail(user != null, user.Email, user.Name, otp, ref bSent);
            if (bSent)
            {
                user.Otp = Convert.ToInt32(otp);
                user.Expireson = DateTime.Now.AddHours(2);
                _context.SaveChanges();
            }
            return bSent ? 1 : 0;
        }
        public CustomerProfile VerifyOTPEmail(string email, int? otp)
        {
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email && x.Otp == otp);
            if (user == null)
                return null;
            user = Authenticate(email);
            return user;
        }
        public CustomerProfile UpdateInfo(CustomerProfile _user)
        {
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == _user.Email);
            if (user == null)
                return null;
            //user.Address=_user.ad
            return user;
        }
        private void SendPasswordEmail(bool bEmailFound, string strEmail, string name, string otp, ref bool bSent)
        {
            try
            {
                if (bEmailFound)
                {
                    using (SmtpClient objSMTP = new SmtpClient(_appSettings.SMTP_Server))
                    //using (SmtpClient objSMTP = new SmtpClient("smtpout.secureserver.net"))
                    {
                        using (MailMessage objMail = new MailMessage())
                        {
                            objMail.To.Add(new MailAddress(strEmail));
                            objMail.From = new MailAddress(_appSettings.Admin_Mail, _appSettings.Admin_Name);
                            objMail.Subject = "One Time Password (OTP) for First Time Registration";
                            objMail.Body = "Dear " + name + ", Your One Time Password (OTP) for First Time Registration is : <p style='font-size: x-large;'><strong>" + otp + "</strong></p>.";
                            objMail.IsBodyHtml = true;
                            objSMTP.Port = _appSettings.SMTP_Port;
                            objSMTP.UseDefaultCredentials = false;
                            objSMTP.Credentials = new System.Net.NetworkCredential(_appSettings.Admin_Mail, _appSettings.Admin_Password);
                            objSMTP.EnableSsl = true;
                            System.Net.ServicePointManager.Expect100Continue = false;
                            //'send the email
                            objSMTP.Send(objMail);
                        }
                    }
                    bSent = true;
                }
            }
            catch (Exception ex)
            {
                Common.WriteLog("Error in sending email\n" + ex.Message);
                //ltEmailSent.Text = "<div class=\"alert alert-error\"><strong> An error has occurred.  Please contact Flying Bridge Technologies for assistance.</strong></div>";
            }
        }
        private string GenerateOTP()
        {
            string numbers = "1234567890";
            int length = int.Parse("6");
            string otp = string.Empty;
            for (int i = 0; i < length; i++)
            {
                string character = string.Empty;
                do
                {
                    int index = new Random().Next(0, 6);
                    character = numbers.ToCharArray()[index].ToString();
                } while (otp.IndexOf(character) != -1);
                otp += character;
            }
            return otp;
        }

        public UserAccount GenerateJWT(string userName, string apikey)
        {
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == userName && x.Apikey == apikey);
            //return null if user is not found
            if (user == null)
                return null;
            //If user is found
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Key);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] {
            new Claim(ClaimTypes.PrimarySid,user.Customerid.ToString()),
            new Claim(ClaimTypes.Name,user.Name.ToString()),
            //new Claim(ClaimTypes.Role,"BankCustomer"),
            //new Claim(ClaimTypes.Version,"V0.1")
            }),
                Expires = DateTime.UtcNow.AddMinutes(60),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            string strToken = tokenHandler.WriteToken(token);
            var _user = _context.CustomerProfiles.SingleOrDefault(x => x.Email == userName);

            return new UserAccount
            {
                customerid = _user.Customerid,
                name = _user.Name,
                email = _user.Email,
                password = "",
                token = strToken

            };
        }

        //public string GenerateJWT(string userName, string apikey, string secretkey)
        //{
        //    var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == userName && x.Secretkey == secretkey && x.Apikey == apikey);
        //    //var user = users.SingleOrDefault(x => x.UserName == userName && x.Password == password);
        //    //return null if user is not found
        //    if (user == null)
        //        return null;
        //    //If user is found
        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var key = Encoding.ASCII.GetBytes(user.Secretkey);
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new Claim[] {
        //    new Claim(ClaimTypes.PrimarySid,user.Customerid.ToString()),
        //    new Claim(ClaimTypes.Name,user.Name.ToString()),
        //    //new Claim(ClaimTypes.Role,"BankCustomer"),
        //    //new Claim(ClaimTypes.Version,"V0.1")
        //    }),
        //        Expires = DateTime.UtcNow.AddMinutes(60),
        //        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        //    };
        //    var token = tokenHandler.CreateToken(tokenDescriptor);
        //    return tokenHandler.WriteToken(token);
        //}
        public CustomerProfile Authenticate(string userName)
        {
            //Generate the code and send it to work profile mail id and then authenticate
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == userName);
            //var user = users.SingleOrDefault(x => x.UserName == userName && x.Password == password);
            //return null if user is not found
            if (user == null)
                return null;
            var key = Encoding.ASCII.GetBytes(_appSettings.Key);
            //var hmac = new HMACSHA256();
            //var _key = Convert.ToBase64String(hmac.Key);
            //user.Secretkey = _appSettings.Key;// Convert.ToBase64String(key);// tokenHandler.WriteToken(token);
            var _apikey = RandomString(100);
            user.Apikey = _apikey;
            BitArray bArr1 = new BitArray(15);
            bArr1.Set(0, true);
            user.Isverified = true;// bArr1;
            var users = _context.CustomerProfiles.Where(usr => usr.Managementid == user.Customerid).ToList();
            users.ForEach(ur => ur.Apikey = _apikey);
            _context.CustomerProfiles.Update(user);
            _context.SaveChanges();
            return user;
        }
        public string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length).Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
