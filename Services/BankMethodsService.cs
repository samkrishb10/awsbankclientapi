﻿using AWSBankClientAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AWSBankClientAPI.Services
{
    public class BankMethodsService : IBankMethodsService
    {
        private readonly AppSettings _appSettings;
        private readonly psqlbankapiContext _context;
        KMSHelper kMSHelper = new KMSHelper();

        public BankMethodsService(IOptions<AppSettings> appSettings, psqlbankapiContext context)
        {
            _appSettings = appSettings.Value;
            _context = context;
        }
        public string Getupdatedsummery(int customerid)
        {
            string accountsummery = string.Empty;
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Customerid == customerid);
            //return null if user is not found
            if (user == null)
                return null;
            //var bankaccountlist = _context.CustomerBankInfos
            //    .Where(bnk => bnk.CustomerId == user.Customerid).ToList()
            //     .SelectMany(bnklist => _context.CustomerAccountSummeries
            //     .Where(account => account.Customerbankid == bnklist.Customerbankid)
            //     .SelectMany(cbnk => _context.BanksInfos
            //     .Where(bank => bank.BankId == bnklist.BankId).DefaultIfEmpty()
            //  .Select(b => new
            //  {
            //      bnklist.BankId,
            //      b.BankName,
            //      bnklist.CustomerId,
            //      bnklist.AccountNo,
            //      bnklist.Branch,
            //      bnklist.Name,
            //      bnklist.Ifsc,
            //      cbnk.Depositbalance,
            //      cbnk.Savingbalance
            //  }))).Distinct().OrderBy(b => b.BankName).ToList();
            //return JsonConvert.SerializeObject((bankaccountlist));
            var bankaccountlist = _context.CustomerBankInfos
               .Where(bnk => bnk.CustomerId == user.Customerid).ToList()
                .SelectMany(bnklist => _context.CustomerAccountSummeries
                .Where(account => account.Customerbankid == bnklist.Customerbankid)
                .SelectMany(cbnk => _context.BanksInfos
                .Where(bank => bank.BankId == bnklist.BankId).DefaultIfEmpty()
             .Select(b => new
             {
                 bnklist.BankId,
                 b.BankName,
                 bnklist.CustomerId,
                 bnklist.AccountNo,
                 bnklist.Branch,
                 bnklist.Name,
                 bnklist.Ifsc,
                 cbnk.Depositbalance,
                 cbnk.Savingbalance
             }))).Distinct().OrderBy(b => b.BankName).ToList();
            return JsonConvert.SerializeObject((bankaccountlist));
        }
        public string Getsummary(string email, int banktypeid)
        {
            string accountsummery = string.Empty;
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email);
            //var user = users.SingleOrDefault(x => x.UserName == userName && x.Password == password);
            //return null if user is not found
            if (user == null)
                return null;
            if (banktypeid != 8)
                accountsummery = Summeryapi(user, banktypeid);
            //else
            //    accountsummery = getBalances(user, banktypeid);
            //accountsummery = Summeryapi(user, banktypeid);
            //var banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid).ToList();
            //if (banklist.Count == 0)
            //    return null;
            //List<UserSummaryRequest> lstusercredentials = new List<UserSummaryRequest>();
            //foreach (CustomerBankInfo item in banklist)
            //{
            //    lstusercredentials.Add(new UserSummaryRequest()
            //    {
            //        username = item.CustomerId.ToString(),
            //        password = item.Password,
            //        data_type = "Summery"
            //    });
            //}
            //UserSuammryRequestsParam users = new UserSuammryRequestsParam() { users = lstusercredentials };
            ////lstusercredentials.pus
            //using (var client = new HttpClient())
            //{
            //    var request = new HttpRequestMessage
            //    {
            //        Method = HttpMethod.Get,
            //        RequestUri = new Uri(_appSettings.bankapiurl + BankAPIS.GetBankURLS(banktypeid)),
            //        //Content = new StringContent("{\"username\":\"63142033\",\"password\":\"Sankar@$04\",\"data_type\":\"Summery\"}", Encoding.UTF8, "application/json")
            //        Content = new StringContent(JsonConvert.SerializeObject(users), Encoding.UTF8, "application/json")

            //    };
            //    var response = client.SendAsync(request);
            //    response.Wait();
            //    var result = response.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        switch ((BankNames)banktypeid)
            //        {
            //            case BankNames.HDFC_Bank:
            //                List<List<HDFCUserSummaryResponse>> hdfcaccountsummery = new List<List<HDFCUserSummaryResponse>>();
            //                var hdfcreadTask = result.Content.ReadAsAsync<List<List<HDFCUserSummaryResponse>>>();
            //                hdfcreadTask.Wait();
            //                hdfcaccountsummery = hdfcreadTask.Result;
            //                accountsummery = JsonConvert.SerializeObject((hdfcaccountsummery.Count > 0 ? hdfcaccountsummery[0] : hdfcaccountsummery));
            //                break;
            //            case BankNames.State_Bank_of_India:
            //                break;
            //            case BankNames.Andhra_Bank:
            //                break;
            //            case BankNames.Axis_Bank:
            //                break;
            //            case BankNames.ICICI_Personal_Bank:
            //                List<List<ICICIUserSummaryResponse>> iciciaccountsummery = new List<List<ICICIUserSummaryResponse>>();
            //                var readTask = result.Content.ReadAsAsync<List<List<ICICIUserSummaryResponse>>>();
            //                readTask.Wait();
            //                iciciaccountsummery = readTask.Result;
            //                accountsummery = JsonConvert.SerializeObject((iciciaccountsummery.Count > 0 ? iciciaccountsummery[0] : iciciaccountsummery));
            //                break;
            //            case BankNames.ICICI_Corporate_Bank:
            //                List<List<ICICICORPUserSummaryResponse>> icicicorpaccountsummery = new List<List<ICICICORPUserSummaryResponse>>();
            //                var icicicorpreadTask = result.Content.ReadAsAsync<List<List<ICICICORPUserSummaryResponse>>>();
            //                icicicorpreadTask.Wait();
            //                icicicorpaccountsummery = icicicorpreadTask.Result;
            //                accountsummery = JsonConvert.SerializeObject((icicicorpaccountsummery.Count > 0 ? icicicorpaccountsummery[0] : icicicorpaccountsummery));
            //                break;
            //            case BankNames.Punjab_National_Bank:
            //                break;
            //            default:
            //                break;
            //        }

            //    }
            //    //var responseInfo = response.Content.ReadAsStringAsync();
            //}
            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(_appSettings.bankapiurl);
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    //client.MaxResponseContentBufferSize = new StringContent("{\"name\":\"John Doe\",\"age\":33}", Encoding.UTF8, "application/json");
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    //HTTP GET
            //    var responseTask = client.GetAsync("api/hdfcbanking");
            //    responseTask.Wait();

            //    var result = responseTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {

            //        var readTask = result.Content.ReadAsAsync<AccountSummery>();
            //        readTask.Wait();
            //        var students = readTask.Result;
            //    }
            //}
            return accountsummery;
        }
        public bool UpdateAccountSummery(string email)
        {
            bool isUpdated = true;
            try
            {
                var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email);
                if (user == null)
                    return false;
                var banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid).Select(b => new
                {
                    b.BankId
                }).Distinct().ToList();
                if (banklist.Count == 0)
                    return false;
                foreach (var item in banklist)
                {
                    //Summeryapi(user, item.BankId);
                    if (item.BankId != 8)
                        Summeryapi(user, item.BankId);
                    //else
                    //    getBalances(user, item.BankId);
                }
            }
            catch (Exception ex)
            {
                isUpdated = false;
            }
            return isUpdated;
        }
        public bool UpdateBankAccountSummery(int customerid, string AccountNo, int BankId)
        {
            bool isUpdated = true;
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Customerid == customerid);
            if (user == null)
                return false;
            var banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == customerid && x.AccountNo == AccountNo && x.BankId == BankId).Distinct().ToList();
            if (banklist.Count == 0)
                return false;
            foreach (var item in banklist)
            {
                //Summeryapi(user, item.BankId, item.Username);
                if (item.BankId != 8)
                    Summeryapi(user, item.BankId, item.Username);
                //else
                //    getBalances(user, item.BankId, item.Username);
            }
            return isUpdated;
        }
        public string GetAccountListsummary(string email)
        {
            //var accountsummery = new JArray(); 
            //List<List<UserSummaryResponse>> accountsummery = new List<List<UserSummaryResponse>>();
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email);
            //return null if user is not found
            if (user == null)
                return null;
            //var banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid).ToList();
            //if (banklist.Count == 0)
            //    return null;
            var bankaccountlist = _context.CustomerBankInfos
                .Where(bnk => bnk.CustomerId == user.Customerid).ToList()
                 .SelectMany(bnklist => _context.CustomerAccountSummeries
                 .Where(account => account.Customerbankid == bnklist.Customerbankid)
                 .SelectMany(cbnk => _context.BanksInfos
                 .Where(bank => bank.BankId == bnklist.BankId).DefaultIfEmpty()
              .Select(b => new
              {
                  bnklist.BankId,
                  b.BankName,
                  bnklist.CustomerId,
                  bnklist.AccountNo,
                  bnklist.Branch,
                  bnklist.Name,
                  bnklist.Ifsc,
                  cbnk.Depositbalance,
                  cbnk.Savingbalance
              }))).Distinct().OrderBy(b => b.BankName).ToList();
            //List<UserSummaryRequest> lstusercredentials = new List<UserSummaryRequest>();
            //foreach (CustomerBankInfo item in banklist)
            //{
            //    lstusercredentials.Add(new UserSummaryRequest()
            //    {
            //        username = item.CustomerId,
            //        password = item.Password,
            //        data_type = "Summery"
            //    });
            //}
            //UserSuammryRequestsParam users = new UserSuammryRequestsParam() { users = lstusercredentials };
            //lstusercredentials.pus
            //using (var client = new HttpClient())
            //{
            //    var request = new HttpRequestMessage
            //    {
            //        Method = HttpMethod.Get,
            //        RequestUri = new Uri(_appSettings.bankapiurl + BankAPIS.GetBankURLS(banktypeid)),
            //        //Content = new StringContent("{\"username\":\"63142033\",\"password\":\"Sankar@$04\",\"data_type\":\"Summery\"}", Encoding.UTF8, "application/json")
            //        Content = new StringContent(JsonConvert.SerializeObject(users), Encoding.UTF8, "application/json")

            //    };
            //    var response = client.SendAsync(request);
            //    response.Wait();
            //    var result = response.Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        //var jobjest=JObject.Parse
            //        //dynamic obj = JsonConvert.DeserializeObject<dynamic>(result.Content.ReadAsAsync((result));
            //        //var readTask = result.Content.ReadAsAsync<dynamic>();
            //        var readTask = result.Content.ReadAsAsync<List<List<UserSummaryResponse>>>();
            //        readTask.Wait();
            //        accountsummery = readTask.Result;
            //    }
            //    //var responseInfo = response.Content.ReadAsStringAsync();
            //}
            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(_appSettings.bankapiurl);
            //    client.DefaultRequestHeaders.Accept.Clear();
            //    //client.MaxResponseContentBufferSize = new StringContent("{\"name\":\"John Doe\",\"age\":33}", Encoding.UTF8, "application/json");
            //    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    //HTTP GET
            //    var responseTask = client.GetAsync("api/hdfcbanking");
            //    responseTask.Wait();

            //    var result = responseTask.Result;
            //    if (result.IsSuccessStatusCode)
            //    {

            //        var readTask = result.Content.ReadAsAsync<AccountSummery>();
            //        readTask.Wait();
            //        var students = readTask.Result;
            //    }
            //}
            return JsonConvert.SerializeObject((bankaccountlist));
        }
        public string GetTransactions(string email, string from_date, string to_date)
        {
            //string accounttransactions = string.Empty;
            List<List<UserTransactResponse>> actransactions = new List<List<UserTransactResponse>>();
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email);
            //var user = users.SingleOrDefault(x => x.UserName == userName && x.Password == password);
            //return null if user is not found
            if (user == null)
                return null;
            var banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid).Distinct().ToList();
            if (banklist.Count == 0)
                return null;
            foreach (var item in banklist)
            {
                if (item.Username != "")
                {
                    List<UserTransactResponse> transactions = new List<UserTransactResponse>();
                    transactions = GetTransactionsAPI(user, item.BankId, from_date, to_date);
                    if (transactions.Count > 0)
                    {
                        actransactions.Add(transactions);
                    }
                }
            }
            return JsonConvert.SerializeObject(actransactions);
        }
        public string GetSpecTransactions(string email, int banktypeid, string from_date, string to_date)
        {
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email);
            //var user = users.SingleOrDefault(x => x.UserName == userName && x.Password == password);
            //return null if user is not found
            if (user == null)
                return null;
            List<UserTransactResponse> transactions = new List<UserTransactResponse>();
            transactions = GetTransactionsAPI(user, banktypeid, from_date, to_date);
            return JsonConvert.SerializeObject(transactions); ;
        }
        private List<UserTransactResponse> GetTransactionsAPI(CustomerProfile user, int banktypeid, string from_date, string to_date)
        {
            List<List<UserTransactResponse>> accounttransactions = new List<List<UserTransactResponse>>();
            var banklist = new List<Secureddata>();
            BankAccount bankAccount = null;
            bankAccount.customerid = (int)user.Customerid;
            bankAccount.bankid = banktypeid;
            banklist = getBankDetails(bankAccount).Result;// _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid).ToList();
            //var banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid).ToList();
            if (banklist.Count == 0)
                return null;
            List<List<UserTransactResponse>> lstAccTransactions = new List<List<UserTransactResponse>>();
            List<UserTransactResponse> lstbnkAccTransactions = new List<UserTransactResponse>();
            List<UserTransactionRequest> lstusertransactdetials = new List<UserTransactionRequest>();
            try
            {
                bool dateconverted = false;
                foreach (Secureddata item in banklist)
                {
                    if (!dateconverted)
                    {
                        switch ((BankNames)item.BankId)
                        {
                            case BankNames.HDFC_Bank:
                                from_date = Convert.ToDateTime(from_date).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                to_date = Convert.ToDateTime(to_date).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                break;
                            case BankNames.State_Bank_of_India:
                                break;
                            case BankNames.Andhra_Bank:
                                from_date = Convert.ToDateTime(from_date).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                to_date = Convert.ToDateTime(to_date).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                break;
                            case BankNames.Axis_Bank:
                                from_date = Convert.ToDateTime(from_date).ToString("dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                to_date = Convert.ToDateTime(to_date).ToString("dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                break;
                            case BankNames.ICICI_Personal_Bank:
                                from_date = Convert.ToDateTime(from_date).ToString("dd,MM,yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                to_date = Convert.ToDateTime(to_date).ToString("dd,MM,yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                break;
                            case BankNames.ICICI_Corporate_Bank:
                                from_date = Convert.ToDateTime(from_date).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                to_date = Convert.ToDateTime(to_date).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                break;
                            case BankNames.Punjab_National_Bank:
                                from_date = Convert.ToDateTime(from_date).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                to_date = Convert.ToDateTime(to_date).ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                break;
                            default:
                                break;
                        }
                        dateconverted = true;
                    }
                    lstusertransactdetials.Add(new UserTransactionRequest()
                    {
                        username = item.Username.ToString(),
                        password = Decrypt(item.Password),
                        data_type = "Transactions",
                        from_date = from_date,
                        to_date = to_date
                    });
                }
                UserTransactionRequestsParam users = new UserTransactionRequestsParam() { users = lstusertransactdetials };
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(_appSettings.bankapiurl + BankAPIS.GetBankURLS(banktypeid)),
                        Content = new StringContent(JsonConvert.SerializeObject(users), Encoding.UTF8, "application/json")
                    };
                    var response = client.SendAsync(request);
                    response.Wait();
                    var result = response.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        if (result.Content.ReadAsStringAsync().Result.IndexOf("errorMessage") != -1)
                        {
                            JObject jobj = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                            _context.Apiloggers.Add(new Apilogger
                            {
                                Message = jobj.SelectToken("errorMessage").ToString(),
                                Innermessage = jobj.SelectToken("stackTrace").ToString(),
                                Statuscode = jobj.SelectToken("errorType").ToString(),
                                Sourceoferror = "AccountTransactionPythonAPI"
                            });
                            _context.SaveChanges();
                        }
                        else
                        {
                            switch ((BankNames)banktypeid)
                            {
                                case BankNames.HDFC_Bank:
                                    List<List<HDFCUserTransactResponse>> hdfcaccounttransactions = new List<List<HDFCUserTransactResponse>>();
                                    var hdfcreadTask = result.Content.ReadAsAsync<List<List<HDFCUserTransactResponse>>>();
                                    hdfcreadTask.Wait();
                                    hdfcaccounttransactions = hdfcreadTask.Result;
                                    lstbnkAccTransactions = new List<UserTransactResponse>();
                                    foreach (List<HDFCUserTransactResponse> item in hdfcaccounttransactions)
                                    {
                                        if (item.Count > 0)
                                        {
                                            foreach (HDFCUserTransactResponse hdfctransact in item)
                                            {
                                                lstbnkAccTransactions.Add(new UserTransactResponse()
                                                {
                                                    Date = hdfctransact.Date,
                                                    Narration = hdfctransact.Narration,
                                                    Withdrawal = hdfctransact.Withdrawal,
                                                    Deposit = hdfctransact.Deposit,
                                                    Closing_Balance = hdfctransact.Closing_Balance,
                                                    Refno = hdfctransact.RefNo,
                                                    Valuedate = hdfctransact.Value_Date,
                                                    customerid = user.Customerid,
                                                    bankid = banktypeid
                                                });
                                            }
                                        }
                                    }
                                    lstAccTransactions.Add(lstbnkAccTransactions);
                                    break;
                                case BankNames.State_Bank_of_India:
                                    break;
                                case BankNames.Andhra_Bank:
                                    List<List<ABUserTransactResponse>> abaccounttransactions = new List<List<ABUserTransactResponse>>();
                                    var abreadTask = result.Content.ReadAsAsync<List<List<ABUserTransactResponse>>>();
                                    abreadTask.Wait();
                                    lstbnkAccTransactions = new List<UserTransactResponse>();
                                    abaccounttransactions = abreadTask.Result;
                                    foreach (List<ABUserTransactResponse> item in abaccounttransactions)
                                    {
                                        if (item.Count > 0)
                                        {
                                            foreach (ABUserTransactResponse abtransact in item)
                                            {
                                                bool isDebit = abtransact.Transaction.Equals("( Cr)") ? false : true;
                                                lstbnkAccTransactions.Add(new UserTransactResponse()
                                                {
                                                    Date = abtransact.Date,
                                                    Narration = abtransact.Remarks,
                                                    Withdrawal = isDebit ? abtransact.Amount : "0",
                                                    Deposit = isDebit ? "0" : abtransact.Amount,
                                                    Closing_Balance = abtransact.Account_Balance,
                                                    Refno = abtransact.Instrument_ID,
                                                    Valuedate = abtransact.Date,
                                                    customerid = user.Customerid,
                                                    bankid = banktypeid
                                                });
                                            }

                                        }
                                    }
                                    lstAccTransactions.Add(lstbnkAccTransactions);
                                    break;
                                case BankNames.Axis_Bank:
                                    List<List<AxisUserTransactResponse>> axisaccounttransactions = new List<List<AxisUserTransactResponse>>();
                                    var axisreadTask = result.Content.ReadAsAsync<List<List<AxisUserTransactResponse>>>();
                                    axisreadTask.Wait();
                                    lstbnkAccTransactions = new List<UserTransactResponse>();
                                    axisaccounttransactions = axisreadTask.Result;
                                    foreach (List<AxisUserTransactResponse> item in axisaccounttransactions)
                                    {
                                        if (item.Count > 0)
                                        {
                                            foreach (AxisUserTransactResponse abtransact in item)
                                            {
                                                if (abtransact.Amount != null)
                                                {
                                                    bool isDebit = abtransact.Transaction.Equals("CR") ? false : true;
                                                    lstbnkAccTransactions.Add(new UserTransactResponse()
                                                    {
                                                        Date = abtransact.Date,
                                                        Narration = abtransact.Remarks,
                                                        Withdrawal = isDebit ? abtransact.Amount : "0",
                                                        Deposit = isDebit ? "0" : abtransact.Amount,
                                                        Closing_Balance = abtransact.Balance,
                                                        Refno = abtransact.Instrument_ID,
                                                        Valuedate = abtransact.Value_Date,
                                                        customerid = user.Customerid,
                                                        bankid = banktypeid
                                                    });
                                                }
                                            }

                                        }
                                    }
                                    lstAccTransactions.Add(lstbnkAccTransactions);
                                    break;
                                case BankNames.ICICI_Personal_Bank:
                                    List<List<ICICIUserTransactResponse>> iciciaccounttransactions = new List<List<ICICIUserTransactResponse>>();
                                    var icicireadTask = result.Content.ReadAsAsync<List<List<ICICIUserTransactResponse>>>();
                                    icicireadTask.Wait();
                                    lstbnkAccTransactions = new List<UserTransactResponse>();
                                    iciciaccounttransactions = icicireadTask.Result;
                                    foreach (List<ICICIUserTransactResponse> item in iciciaccounttransactions)
                                    {
                                        if (item.Count > 0)
                                        {
                                            foreach (ICICIUserTransactResponse icicitransact in item)
                                            {
                                                lstbnkAccTransactions.Add(new UserTransactResponse()
                                                {
                                                    Date = icicitransact.Transaction_Date,
                                                    Narration = "",
                                                    Withdrawal = icicitransact.Withdrawal_Amount,
                                                    Deposit = icicitransact.Deposit_Amount,
                                                    Closing_Balance = icicitransact.Balance,
                                                    Refno = icicitransact.S_No,
                                                    Valuedate = icicitransact.Value_Date,
                                                    customerid = user.Customerid,
                                                    bankid = banktypeid
                                                });
                                            }

                                        }
                                    }
                                    lstAccTransactions.Add(lstbnkAccTransactions);
                                    break;
                                case BankNames.ICICI_Corporate_Bank:
                                    List<List<ICICICORPUserTransactResponse>> icicicorpaccounttransactions = new List<List<ICICICORPUserTransactResponse>>();
                                    var icicicorpreadTask = result.Content.ReadAsAsync<List<List<ICICICORPUserTransactResponse>>>();
                                    icicicorpreadTask.Wait();
                                    lstbnkAccTransactions = new List<UserTransactResponse>();
                                    icicicorpaccounttransactions = icicicorpreadTask.Result;
                                    foreach (List<ICICICORPUserTransactResponse> item in icicicorpaccounttransactions)
                                    {
                                        if (item.Count > 0)
                                        {
                                            foreach (ICICICORPUserTransactResponse icicitransact in item)
                                            {
                                                lstbnkAccTransactions.Add(new UserTransactResponse()
                                                {
                                                    Date = icicitransact.Transaction_Date,
                                                    Narration = icicitransact.Description,
                                                    Withdrawal = icicitransact.Debit_Amount,
                                                    Deposit = icicitransact.Credit_Amount,
                                                    Closing_Balance = icicitransact.Account_Balance,
                                                    Refno = icicitransact.Instruments_ID,
                                                    Valuedate = icicitransact.Value_Date,
                                                    customerid = user.Customerid,
                                                    bankid = banktypeid
                                                });
                                            }

                                        }
                                    }
                                    lstAccTransactions.Add(lstbnkAccTransactions);
                                    break;
                                case BankNames.Punjab_National_Bank:
                                    List<List<PNBUserTransactResponse>> pnbaccounttransactions = new List<List<PNBUserTransactResponse>>();
                                    var pnbreadTask = result.Content.ReadAsAsync<List<List<PNBUserTransactResponse>>>();
                                    pnbreadTask.Wait();
                                    lstbnkAccTransactions = new List<UserTransactResponse>();
                                    pnbaccounttransactions = pnbreadTask.Result;
                                    foreach (List<PNBUserTransactResponse> item in pnbaccounttransactions)
                                    {
                                        if (item.Count > 0)
                                        {
                                            foreach (PNBUserTransactResponse pnbtransact in item)
                                            {
                                                lstbnkAccTransactions.Add(new UserTransactResponse()
                                                {
                                                    Date = pnbtransact.Transaction_Date,
                                                    Narration = pnbtransact.Transaction_Remarks,
                                                    Withdrawal = pnbtransact.Debit,
                                                    Deposit = pnbtransact.Credit,
                                                    Closing_Balance = pnbtransact.Account_Balance,
                                                    Refno = pnbtransact.Instrument_ID,
                                                    Valuedate = pnbtransact.Transaction_Date,
                                                    customerid = user.Customerid,
                                                    bankid = banktypeid
                                                });
                                            }

                                        }
                                    }
                                    lstAccTransactions.Add(lstbnkAccTransactions);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _context.Apiloggers.Add(new Apilogger
                {
                    Message = ex.Message,
                    Innermessage = ex.InnerException.Message,
                    Statuscode = "",
                    Sourceoferror = "AccountTransactions"
                });
                _context.SaveChanges();
                return new List<UserTransactResponse>();
            }
            bool isadded = false;
            foreach (List<UserTransactResponse> item in lstAccTransactions)
            {
                if (item.Count > 0)
                {
                    foreach (var lstTransact in item)
                    {
                        var lstcbank = _context.CustomerBankInfos.Where(x => x.CustomerId == lstTransact.customerid && x.BankId == lstTransact.bankid).ToList();
                        if (lstcbank.Count == 0)
                            continue;
                        foreach (CustomerBankInfo cbi in lstcbank)
                        {
                            DateTime dtval = DateTime.Now;
                            dtval = (BankNames)lstTransact.bankid != (BankNames.Axis_Bank) ? (lstTransact.Date != "" ? Convert.ToDateTime(lstTransact.Date.Trim(), System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat) : DateTime.Now) : (lstTransact.Date != "" ? Convert.ToDateTime(lstTransact.Date.Replace(" ", "").Replace("'", "").Trim(), System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat) : DateTime.Now);
                            var lstcbanktransactions = _context.CustomerAccountTransactions.Where(x => x.Customerbankid == cbi.Customerbankid && x.Date == dtval.Date && x.Deposit == lstTransact.Deposit && x.Withdrawal == lstTransact.Withdrawal && x.ClosingBalance == lstTransact.Closing_Balance).ToList();
                            if (lstcbanktransactions.Count == 0)
                            {
                                if (lstTransact.Closing_Balance != null)
                                {
                                    var t = new CustomerAccountTransaction
                                    {
                                        Refno = lstTransact.Refno,
                                        Deposit = lstTransact.Deposit,
                                        ClosingBalance = lstTransact.Closing_Balance,
                                        ValueDate = (BankNames)lstTransact.bankid != (BankNames.Axis_Bank) ? (lstTransact.Valuedate != "" ? Convert.ToDateTime(lstTransact.Valuedate.Trim(), System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat) : DateTime.Now) : (lstTransact.Valuedate != "" ? Convert.ToDateTime(lstTransact.Valuedate.Replace(" ", "").Replace("'", "").Trim(), System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat) : DateTime.Now),
                                        Customerbankid = cbi.Customerbankid,
                                        Withdrawal = lstTransact.Withdrawal,
                                        Date = (BankNames)lstTransact.bankid != (BankNames.Axis_Bank) ? (lstTransact.Date != "" ? Convert.ToDateTime(lstTransact.Date.Trim(), System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat) : DateTime.Now) : (lstTransact.Date != "" ? Convert.ToDateTime(lstTransact.Date.Replace(" ", "").Replace("'", "").Trim(), System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat) : DateTime.Now),
                                        Narration = lstTransact.Narration
                                    };
                                    isadded = true;
                                    _context.CustomerAccountTransactions.Add(t);
                                }
                            }
                            else
                            {
                                if (lstTransact.Closing_Balance != null)
                                {
                                    lstcbanktransactions[0].Refno = lstTransact.Refno;
                                    lstcbanktransactions[0].Deposit = lstTransact.Deposit;
                                    lstcbanktransactions[0].ClosingBalance = lstTransact.Closing_Balance;
                                    lstcbanktransactions[0].ValueDate = Convert.ToDateTime(lstTransact.Valuedate, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat);
                                    lstcbanktransactions[0].Customerbankid = cbi.Customerbankid;
                                    lstcbanktransactions[0].Withdrawal = lstTransact.Withdrawal;
                                    lstcbanktransactions[0].Date = Convert.ToDateTime(lstTransact.Date, System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat);
                                    lstcbanktransactions[0].Narration = lstTransact.Narration;
                                    _context.CustomerAccountTransactions.Attach(lstcbanktransactions[0]);
                                    isadded = true;
                                    _context.Entry(lstcbanktransactions[0]).State = EntityState.Modified;
                                }
                            }
                        }
                    }
                }
            }
            if (isadded)
                _context.SaveChanges();
            return lstbnkAccTransactions; //JsonConvert.SerializeObject((accounttransactions.Count > 0 ? accounttransactions[0] : accounttransactions));
        }
        public string GetAllTransactions(string email, string from_date, string to_date, int _bankid)
        {
            //int _bankid = Convert.ToInt32(bankid);
            DateTime _from_date = Convert.ToDateTime(from_date);
            DateTime _to_date = Convert.ToDateTime(to_date);
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email);
            //return null if user is not found
            if (user == null)
                return null;
            var bankaccountlist = _context.CustomerBankInfos
                .Where(bnk => bnk.CustomerId == user.Customerid && bnk.BankId == (_bankid > 0 ? _bankid : bnk.BankId)).ToList()
                 .SelectMany(bnklist => _context.CustomerAccountTransactions
                 .Where(account => account.Customerbankid == bnklist.Customerbankid && account.ValueDate >= _from_date && account.ValueDate <= _to_date)
                 .SelectMany(cbnk => _context.BanksInfos
                 .Where(bank => bank.BankId == (_bankid > 0 ? _bankid : bnklist.BankId)).DefaultIfEmpty()
              .Select(b => new
              {
                  bnklist.BankId,
                  cbnk.CustomerTransactionId,
                  b.BankName,
                  bnklist.CustomerId,
                  bnklist.AccountNo,
                  //bnklist.Branch,
                  bnklist.Name,
                  cbnk.Createddate,
                  cbnk.Narration,
                  cbnk.Refno,
                  cbnk.ValueDate,
                  cbnk.Withdrawal,
                  cbnk.Deposit,
                  cbnk.ClosingBalance,
              }))).Distinct().ToList();
            bankaccountlist = bankaccountlist.OrderByDescending(b => b.ValueDate).ToList();//.ThenByDescending(b => b.Createddate).OrderByDescending(b => b.CustomerTransactionId).ToList();
            //if (bankaccountlist.Count > 0 && _bankid > 0)
            //    bankaccountlist = bankaccountlist.Where(x => x.BankId == _bankid).ToList().OrderByDescending(b => b.ValueDate).ThenBy(b => b.BankName).ThenByDescending(b => b.Createddate).OrderByDescending(b => b.CustomerTransactionId).ToList();
            return JsonConvert.SerializeObject((bankaccountlist));
        }
        public string GetTransactionsbycategory(string email, string from_date, string to_date, int _bankid)
        {
            DateTime _from_date = Convert.ToDateTime(from_date);
            DateTime _to_date = Convert.ToDateTime(to_date);
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Email == email);
            //return null if user is not found
            if (user == null)
                return null;
            var bankaccountlist = _context.CustomerBankSecureds
                .Where(bnk => bnk.CustomerId == user.Customerid && bnk.BankId == (_bankid > 0 ? _bankid : bnk.BankId)).ToList()
                 .SelectMany(bnklist => _context.CustomerAccountTransactionAnalyses
                 .Where(account => account.Customerbankid == bnklist.Customerbankid && account.ValueDate >= _from_date && account.ValueDate <= _to_date)
                 .SelectMany(cbnk => _context.BanksInfos
                 .Where(bank => bank.BankId == (_bankid > 0 ? _bankid : bnklist.BankId)).DefaultIfEmpty()
              .Select(b => new
              {
                  bnklist.BankId,
                  cbnk.CustomerTransactionId,
                  b.BankName,
                  bnklist.CustomerId,
                  bnklist.AccountNo,
                  bnklist.Name,
                  //cbnk.Createddate,
                  cbnk.Label,
                  cbnk.Narration,
                  cbnk.Refno,
                  cbnk.ValueDate,
                  cbnk.Withdrawal,
                  cbnk.Deposit,
                  cbnk.ClosingBalance,
              }))).Distinct().ToList();
            bankaccountlist = bankaccountlist.OrderByDescending(b => b.ValueDate).ToList();
            return JsonConvert.SerializeObject((bankaccountlist));
        }
        public async Task<List<Secureddata>> getBankDetails(BankAccount ba)
        {
            var _bankaccount = _context.CustomerBankSecureds.Where(x => x.CustomerId == ba.customerid && x.BankId == ba.bankid).ToList();
            List<Secureddata> secureddatas = new List<Secureddata>();
            foreach (CustomerBankSecured customerBankSecured in _bankaccount)
            {
                Secureddata secureddata = null;
                MemoryStream memoryStream = await kMSHelper.DecryptDataWithKey(new MemoryStream(customerBankSecured.Logindata), _appSettings.MasterKey);
                LoginCred newDog = (LoginCred)CommonMethods.DeserializeFromStream(memoryStream);
                if (!string.IsNullOrEmpty(ba.username))
                {
                    if (newDog.Username == ba.username)
                    {
                        secureddata.Username = newDog.Username;
                        secureddata.Password = newDog.Password;
                        secureddata.CustomerBankId = customerBankSecured.Customerbankid;
                        secureddata.BankId = customerBankSecured.BankId;
                        secureddatas.Add(secureddata);
                        break;
                    }
                }
                else
                {
                    secureddata.Username = newDog.Username;
                    secureddata.Password = newDog.Password;
                    secureddata.CustomerBankId = customerBankSecured.Customerbankid;
                    secureddata.BankId = customerBankSecured.BankId;
                    secureddatas.Add(secureddata);
                }
            }
            // var _bankaccount = _context.CustomerBankInfos.Where(x => x.CustomerId == ba.customerid && x.BankId == ba.bankid && x.Username == ba.username).ToList();
            return secureddatas;// _bankaccount.Count != 0 ? true : false;
        }
        public async Task<int> AddBank(BankAccount ba)
        {
            //var _customerbank = new CustomerBankInfo
            //{
            //    CustomerId = ba.customerid,
            //    BankId = ba.bankid,
            //    Username = ba.username,
            //    AccountType = "",
            //    AccountNo = "",
            //    Password = Encrypt(ba.password)
            //};
            //_context.CustomerBankInfos.Add(_customerbank);
            //_context.SaveChanges();
            LoginCred cred = new LoginCred
            {
                Username = ba.username,
                Password = Encrypt(ba.password)
            };
            byte[] bytes;
            bytes = CommonMethods.ConvertObjToBytes(cred);
            MemoryStream encryptedText = await kMSHelper.EncryptDataWithKey(bytes, _appSettings.MasterKey);
            byte[] encryptbytes = encryptedText.ToArray();
            var _customerbanksecured = new CustomerBankSecured
            {
                CustomerId = ba.customerid,
                BankId = ba.bankid,
                AccountType = "",
                AccountNo = "",
                Logindata = encryptbytes
            };
            _context.CustomerBankSecureds.Add(_customerbanksecured);
            _context.SaveChanges();
            List<Secureddata> customerBank = getBankDetails(ba).Result;
            //int customerBankId = CheckBankDetails(ba).Result;
            //var bankaccountid = _context.CustomerBankInfos.SingleOrDefault(x => x.CustomerId == ba.customerid && x.BankId == ba.bankid && x.Username == ba.username);
            if (customerBank.Count > 0)
            {
                var _customerAcSummery = new CustomerAccountSummery
                {
                    Customerbankid = customerBank[0].CustomerBankId,//bankaccountid.Customerbankid,
                    Savingbalance = 0.0M,
                    Depositbalance = 0.0M
                };
                _context.CustomerAccountSummeries.Add(_customerAcSummery);
                _context.SaveChanges();
            }
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Customerid == ba.customerid);
            if (user != null)
                if (ba.bankid != 8)
                    Summeryapi(user, ba.bankid, ba.username);
            //else
            //    UBPSummery(user, ba.bankid, ba.username);
            return customerBank.Count > 0 ? customerBank[0].CustomerBankId : 0;// bankaccountid != null ? bankaccountid.Customerbankid : 0;
        }
        public int Updateotp(int CustomerId, int BankId, string Username, int otp)
        {
            if (otp > 0)
            {
                var _customerBankInfo = _context.CustomerBankInfos.SingleOrDefault(x => x.CustomerId == CustomerId && x.BankId == BankId && x.Username == Username);
                _customerBankInfo.Otp = otp;
                _context.CustomerBankInfos.Attach(_customerBankInfo);
                _context.Entry(_customerBankInfo).State = EntityState.Modified;
                _context.SaveChanges();
                return 1;
            }
            else
                return 0;
        }
        public int DeleteBank(int customerId, int bankId)
        {
            var bankaccountid = _context.CustomerBankInfos.SingleOrDefault(x => x.CustomerId == customerId && x.BankId == bankId);
            if (bankaccountid.Customerbankid > 0)
            {
                var bankaccountsummery = _context.CustomerAccountSummeries.SingleOrDefault(x => x.Customerbankid == bankaccountid.Customerbankid);
                if (bankaccountsummery != null)
                {
                    _context.CustomerAccountSummeries.Remove(bankaccountsummery);
                    _context.Entry(bankaccountsummery).State = EntityState.Deleted;
                    _context.SaveChanges();
                }
                List<CustomerAccountTransaction> bankaccounttransactions = _context.CustomerAccountTransactions.Where(x => x.Customerbankid == bankaccountid.Customerbankid).ToList();
                if (bankaccounttransactions != null)
                {
                    _context.CustomerAccountTransactions.RemoveRange(bankaccounttransactions);
                    //_context.Entry(bankaccounttransactions).State = EntityState.Deleted;
                    _context.SaveChanges();
                }
                _context.CustomerBankInfos.Remove(bankaccountid);
                _context.Entry(bankaccountid).State = EntityState.Deleted;
                _context.SaveChanges();
                return 1;
            }
            return 0;
        }
        public int InitTransactiondata(int customerid, int bankid)
        {
            var user = _context.CustomerProfiles.FirstOrDefault(x => x.Customerid == customerid);
            //return null if user is not found
            if (user == null)
                return 0;
            string startdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddMonths(-1).ToShortDateString();
            string enddate = DateTime.Now.Date.ToShortDateString();
            GetTransactionsAPI(user, bankid, startdate, enddate);
            return 0;
        }
        private string Summeryapi(CustomerProfile user, int banktypeid, string username = "")
        {
            string accountsummery = string.Empty;
            var banklist = new List<Secureddata>();
            BankAccount bankAccount = null;
            bankAccount.customerid = (int)user.Customerid;
            bankAccount.bankid = banktypeid;
            if (!string.IsNullOrWhiteSpace(username))
            {
                // banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid && x.Username == username).ToList();
                bankAccount.username = username;
                banklist = getBankDetails(bankAccount).Result;
            }
            else
                banklist = getBankDetails(bankAccount).Result;// _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid).ToList();
            if (banklist.Count == 0)
                return null;
            List<UserSummaryResponse> lstAccSummery = new List<UserSummaryResponse>();

            object users;
            try
            {
                if (banktypeid == 2)
                {
                    List<SBIUserSummaryRequest> lstusercredentials = new List<SBIUserSummaryRequest>();
                    foreach (Secureddata item in banklist)
                    {
                        lstusercredentials.Add(new SBIUserSummaryRequest()
                        {
                            customer_id = user.Customerid,
                            username = item.Username.ToString(),
                            password = Decrypt(item.Password),
                            data_type = "Summery"
                        });
                    }
                    users = new SBIUserSummeryRequestsParam() { users = lstusercredentials };
                }
                else
                {
                    List<UserSummaryRequest> lstusercredentials = new List<UserSummaryRequest>();
                    foreach (Secureddata item in banklist)
                    {
                        lstusercredentials.Add(
                            new UserSummaryRequest()
                            {
                                username = item.Username.ToString(),
                                password = Decrypt(item.Password),
                                data_type = "Summery"
                            });
                    }
                    users = new UserSummeryRequestsParam() { users = lstusercredentials };

                }
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage
                    {
                        Method = HttpMethod.Get,
                        RequestUri = new Uri(_appSettings.bankapiurl + BankAPIS.GetBankURLS(banktypeid)),
                        Content = new StringContent(JsonConvert.SerializeObject(users), Encoding.UTF8, "application/json")

                    };
                    var response = client.SendAsync(request);
                    response.Wait();
                    var result = response.Result;
                    if (result.IsSuccessStatusCode)
                    {

                        if (result.Content.ReadAsStringAsync().Result.IndexOf("errorMessage") != -1)
                        {
                            JObject jobj = JObject.Parse(result.Content.ReadAsStringAsync().Result);
                            _context.Apiloggers.Add(new Apilogger
                            {
                                Message = jobj.SelectToken("errorMessage").ToString(),
                                Innermessage = jobj.SelectToken("stackTrace").ToString(),
                                Statuscode = jobj.SelectToken("errorType").ToString(),
                                Sourceoferror = "AccountSummeryPythonAPI"
                            });
                            _context.SaveChanges();
                        }
                        else
                        {
                            switch ((BankNames)banktypeid)
                            {
                                case BankNames.HDFC_Bank:
                                    List<List<HDFCUserSummaryResponse>> hdfcaccountsummery = new List<List<HDFCUserSummaryResponse>>();
                                    var hdfcreadTask = result.Content.ReadAsAsync<List<List<HDFCUserSummaryResponse>>>();
                                    hdfcreadTask.Wait();
                                    hdfcaccountsummery = hdfcreadTask.Result;
                                    foreach (List<HDFCUserSummaryResponse> item in hdfcaccountsummery)
                                    {
                                        if (item.Count > 0)
                                        {
                                            lstAccSummery.Add(new UserSummaryResponse()
                                            {
                                                savingbalance = Convert.ToDecimal(item[0].Available_Balance),
                                                depositbalance = 0.0M,
                                                name = item[0].Name,
                                                branch_address = item[0].Branch,
                                                ifsc_code = "",
                                                account_number = item[0].Account_No,
                                                customerid = user.Customerid,
                                                bankid = banktypeid
                                            });
                                        }
                                    }
                                    accountsummery = JsonConvert.SerializeObject((hdfcaccountsummery.Count > 0 ? hdfcaccountsummery[0] : hdfcaccountsummery));
                                    break;
                                case BankNames.State_Bank_of_India:
                                    List<List<SBIUserSummaryResponse>> sbiaccountsummery = new List<List<SBIUserSummaryResponse>>();
                                    var sbireadTask = result.Content.ReadAsAsync<List<List<SBIUserSummaryResponse>>>();
                                    sbireadTask.Wait();
                                    sbiaccountsummery = sbireadTask.Result;
                                    foreach (List<SBIUserSummaryResponse> item in sbiaccountsummery)
                                    {
                                        if (item.Count > 0)
                                        {
                                            lstAccSummery.Add(new UserSummaryResponse()
                                            {
                                                savingbalance = Convert.ToDecimal(item[0].Available_Balance.Replace("INR", "").Trim()),
                                                depositbalance = 0.0M,
                                                name = "",
                                                branch_address = item[0].Branch,
                                                ifsc_code = "",
                                                account_number = item[0].Account_No,
                                                customerid = user.Customerid,
                                                bankid = banktypeid
                                            });
                                        }
                                    }
                                    accountsummery = JsonConvert.SerializeObject((sbiaccountsummery.Count > 0 ? sbiaccountsummery[0] : sbiaccountsummery));
                                    break;
                                case BankNames.Andhra_Bank:
                                    List<List<ABUserSummaryResponse>> abaccountsummery = new List<List<ABUserSummaryResponse>>();
                                    var abreadTask = result.Content.ReadAsAsync<List<List<ABUserSummaryResponse>>>();
                                    abreadTask.Wait();
                                    abaccountsummery = abreadTask.Result;
                                    foreach (List<ABUserSummaryResponse> item in abaccountsummery)
                                    {
                                        if (item.Count > 0)
                                        {
                                            lstAccSummery.Add(new UserSummaryResponse()
                                            {
                                                savingbalance = Convert.ToDecimal(item[0].Effective_Available_Balance.Replace("INR", "").Trim()),
                                                depositbalance = 0.0M,
                                                name = item[0].Name,
                                                branch_address = item[0].Branch,
                                                ifsc_code = "",
                                                account_number = item[0].Number,
                                                customerid = user.Customerid,
                                                bankid = banktypeid
                                            });
                                        }
                                    }
                                    accountsummery = JsonConvert.SerializeObject((abaccountsummery.Count > 0 ? abaccountsummery[0] : abaccountsummery));
                                    break;
                                case BankNames.Axis_Bank:
                                    List<List<AxisUserSummaryResponse>> axisaccountsummery = new List<List<AxisUserSummaryResponse>>();
                                    var axisreadTask = result.Content.ReadAsAsync<List<List<AxisUserSummaryResponse>>>();
                                    axisreadTask.Wait();
                                    axisaccountsummery = axisreadTask.Result;
                                    foreach (List<AxisUserSummaryResponse> item in axisaccountsummery)
                                    {
                                        if (item.Count > 0)
                                        {
                                            lstAccSummery.Add(new UserSummaryResponse()
                                            {
                                                savingbalance = Convert.ToDecimal(item[0].Effective_Available_Balance.Trim()),
                                                depositbalance = 0.0M,
                                                name = item[0].Name,
                                                branch_address = item[0].Branch,
                                                ifsc_code = "",
                                                account_number = item[0].Number,
                                                customerid = user.Customerid,
                                                bankid = banktypeid
                                            });
                                        }
                                    }
                                    accountsummery = JsonConvert.SerializeObject((axisaccountsummery.Count > 0 ? axisaccountsummery[0] : axisaccountsummery));
                                    break;
                                case BankNames.ICICI_Personal_Bank:
                                    List<List<ICICIUserSummaryResponse>> iciciaccountsummery = new List<List<ICICIUserSummaryResponse>>();
                                    var readTask = result.Content.ReadAsAsync<List<List<ICICIUserSummaryResponse>>>();
                                    readTask.Wait();
                                    iciciaccountsummery = readTask.Result;
                                    foreach (List<ICICIUserSummaryResponse> item in iciciaccountsummery)
                                    {
                                        if (item.Count > 0)
                                        {
                                            lstAccSummery.Add(new UserSummaryResponse()
                                            {
                                                savingbalance = Convert.ToDecimal(item[0].All_Bank_Accounts),
                                                depositbalance = Convert.ToDecimal(item[0].All_Deposit_Accounts),
                                                name = "",
                                                branch_address = item[0].Branch_Address,
                                                ifsc_code = item[0].IFSC_Code,
                                                account_number = item[0].Account_Number,
                                                customerid = user.Customerid,
                                                bankid = banktypeid
                                            });
                                        }
                                    }
                                    accountsummery = JsonConvert.SerializeObject((iciciaccountsummery.Count > 0 ? iciciaccountsummery[0] : iciciaccountsummery));
                                    break;
                                case BankNames.ICICI_Corporate_Bank:
                                    List<List<ICICICORPUserSummaryResponse>> icicicorpaccountsummery = new List<List<ICICICORPUserSummaryResponse>>();
                                    var icicicorpreadTask = result.Content.ReadAsAsync<List<List<ICICICORPUserSummaryResponse>>>();
                                    icicicorpreadTask.Wait();
                                    icicicorpaccountsummery = icicicorpreadTask.Result;
                                    foreach (List<ICICICORPUserSummaryResponse> item in icicicorpaccountsummery)
                                    {
                                        if (item.Count > 0)
                                        {
                                            lstAccSummery.Add(new UserSummaryResponse()
                                            {
                                                savingbalance = Convert.ToDecimal(item[0].EFFECTIVE_AVAILABLE_BALANCE.Replace("INR", "").Trim()),
                                                depositbalance = 0.0M,
                                                name = "",
                                                branch_address = "",
                                                ifsc_code = "",
                                                account_number = item[0].Account_Number,
                                                customerid = user.Customerid,
                                                bankid = banktypeid
                                            });
                                        }
                                    }
                                    accountsummery = JsonConvert.SerializeObject((icicicorpaccountsummery.Count > 0 ? icicicorpaccountsummery[0] : icicicorpaccountsummery));
                                    break;
                                case BankNames.Punjab_National_Bank:
                                    List<List<PNBUserSummaryResponse>> pnbaccountsummery = new List<List<PNBUserSummaryResponse>>();
                                    var pnbreadTask = result.Content.ReadAsAsync<List<List<PNBUserSummaryResponse>>>();
                                    pnbreadTask.Wait();
                                    pnbaccountsummery = pnbreadTask.Result;
                                    foreach (List<PNBUserSummaryResponse> item in pnbaccountsummery)
                                    {
                                        if (item.Count > 0)
                                        {
                                            lstAccSummery.Add(new UserSummaryResponse()
                                            {
                                                savingbalance = Convert.ToDecimal(item[0].Effective_Available_Balance.Replace("Cr.", "").Replace("INR", "").Trim()),
                                                depositbalance = 0.0M,
                                                name = item[0].Account_Name,
                                                branch_address = item[0].Branch,
                                                ifsc_code = "",
                                                account_number = item[0].Account_Number,
                                                customerid = user.Customerid,
                                                bankid = banktypeid
                                            });
                                        }
                                    }
                                    accountsummery = JsonConvert.SerializeObject((pnbaccountsummery.Count > 0 ? pnbaccountsummery[0] : pnbaccountsummery));
                                    break;
                                case BankNames.LIC_Housing_Finance:
                                    List<List<LICHFINUserSummaryResponse>> lichfinaccountsummery = new List<List<LICHFINUserSummaryResponse>>();
                                    var lichfinreadTask = result.Content.ReadAsAsync<List<List<LICHFINUserSummaryResponse>>>();
                                    lichfinreadTask.Wait();
                                    lichfinaccountsummery = lichfinreadTask.Result;
                                    foreach (List<LICHFINUserSummaryResponse> item in lichfinaccountsummery)
                                    {
                                        if (item.Count > 0)
                                        {
                                            lstAccSummery.Add(new UserSummaryResponse()
                                            {
                                                savingbalance = Convert.ToDecimal(item[0].Available_Balance.Trim()),
                                                depositbalance = 0.0M,
                                                name = item[0].Name,
                                                branch_address = item[0].Branch,
                                                ifsc_code = "",
                                                account_number = item[0].Number,
                                                customerid = user.Customerid,
                                                bankid = banktypeid
                                            });
                                        }
                                    }
                                    accountsummery = JsonConvert.SerializeObject((lichfinaccountsummery.Count > 0 ? lichfinaccountsummery[0] : lichfinaccountsummery));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _context.Apiloggers.Add(new Apilogger
                {
                    Message = ex.Message,
                    Innermessage = ex.InnerException.Message,
                    Statuscode = "",
                    Sourceoferror = "AccountSummery"
                });
                _context.SaveChanges();
                return "";
            }
            int _index = 0;
            foreach (UserSummaryResponse item in lstAccSummery)
            {
                var lstcbank = _context.CustomerBankInfos.Where(x => x.CustomerId == item.customerid && x.BankId == item.bankid && x.Username == banklist[_index].Username).ToList();
                _index = _index + 1;
                if (lstcbank.Count == 0)
                    continue;
                foreach (CustomerBankInfo cbi in lstcbank)
                {
                    cbi.Branch = item.branch_address;
                    cbi.Ifsc = item.ifsc_code;
                    cbi.AccountNo = item.account_number;
                    cbi.Name = item.name;
                    _context.CustomerBankInfos.Attach(cbi);
                    _context.Entry(cbi).State = EntityState.Modified;
                    var lstcbanksummery = _context.CustomerAccountSummeries.SingleOrDefault(x => x.Customerbankid == cbi.Customerbankid);
                    if (lstcbanksummery != null)
                    {
                        lstcbanksummery.Savingbalance = item.savingbalance;
                        lstcbanksummery.Depositbalance = item.depositbalance;
                        _context.CustomerAccountSummeries.Attach(lstcbanksummery);
                        _context.Entry(lstcbanksummery).State = EntityState.Modified;
                    }
                    else
                    {
                        var t = new CustomerAccountSummery
                        {
                            Savingbalance = item.savingbalance,
                            Depositbalance = item.depositbalance,
                            Customerbankid = cbi.Customerbankid
                        };
                        //lstcbanksummery.Savingbalance = item.savingbalance;
                        //lstcbanksummery.Depositbalance = item.depositbalance;
                        _context.CustomerAccountSummeries.Add(t);
                        //_context.Entry(lstcbanksummery).State = EntityState.Added;
                    }
                    _context.SaveChanges();
                }
            }
            return accountsummery;
        }
        //private string getBalances(CustomerProfile user, int banktypeid, string username = "")
        //{
        //    string accountsummery = string.Empty;
        //    var banklist = new List<CustomerBankInfo>();
        //    if (!string.IsNullOrWhiteSpace(username))
        //        banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid && x.Username == username).ToList();
        //    else
        //        banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid).ToList();
        //    if (banklist.Count == 0)
        //        return null;
        //    List<UserSummaryResponse> lstAccSummery = new List<UserSummaryResponse>();
        //    UBPSummaryRequest usercredentials = new UBPSummaryRequest();
        //    for (int i = 0; i < banklist.Count; i++)
        //    {
        //        try
        //        {
        //            //usercredentials.username = banklist[i].Username.ToString();
        //            //usercredentials.password = Decrypt(banklist[i].Password);
        //            //usercredentials.account_name = "Sample Sandbox Account";
        //            using (var client = new HttpClient())
        //            {
        //                var request = new HttpRequestMessage
        //                {
        //                    Method = HttpMethod.Get,
        //                    RequestUri = new Uri(_appSettings.UBP_url + "accounts/v2/balances/" + banklist[i].AccountNo)
        //                };
        //                client.DefaultRequestHeaders.Accept.Clear();
        //                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        //                //request.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        //                client.DefaultRequestHeaders.Add("x-ibm-client-id", _appSettings.UBP_client_id);
        //                client.DefaultRequestHeaders.Add("x-ibm-client-secret", _appSettings.UBP_client_secret);
        //                var response = client.SendAsync(request);
        //                response.Wait();
        //                var result = response.Result;
        //                if (result.IsSuccessStatusCode)
        //                {
        //                    if (result.Content.ReadAsStringAsync().Result.IndexOf("errors") != -1)
        //                    {
        //                        JObject jobj = JObject.Parse(result.Content.ReadAsStringAsync().Result);
        //                        _context.Apiloggers.Add(new Apilogger
        //                        {
        //                            Message = jobj.SelectToken("errors[0].message").ToString(),
        //                            Statuscode = jobj.SelectToken("errors[0].code").ToString(),
        //                            Innermessage = jobj.SelectToken("errors[0].description").ToString(),
        //                            Sourceoferror = "UBPBalanceAPI"
        //                        });
        //                        _context.SaveChanges();
        //                    }
        //                    else
        //                    {
        //                        switch ((BankNames)banktypeid)
        //                        {
        //                            case BankNames.Union_Bank_Of_Philippines:
        //                                List<UBP_BalanceResp> _ubp_accountbalanceresponse = new List<UBP_BalanceResp>();
        //                                var ubp_accountbalanceresponseTask = result.Content.ReadAsAsync<List<UBP_BalanceResp>>();
        //                                ubp_accountbalanceresponseTask.Wait();
        //                                _ubp_accountbalanceresponse = ubp_accountbalanceresponseTask.Result;
        //                                lstAccSummery.Add(new UserSummaryResponse()
        //                                {
        //                                    savingbalance = Convert.ToDecimal(_ubp_accountbalanceresponse[0].amount),
        //                                    depositbalance = 0.0M,
        //                                    name = banklist[i].Name,
        //                                    branch_address = "",
        //                                    ifsc_code = "",
        //                                    account_number = banklist[i].AccountNo,
        //                                    customerid = user.Customerid,
        //                                    bankid = banktypeid
        //                                });
        //                                accountsummery = JsonConvert.SerializeObject((lstAccSummery.Count > 0 ? lstAccSummery[0] : lstAccSummery));
        //                                break;
        //                            default:
        //                                break;
        //                        }
        //                    }
        //                }
        //                else if (result.StatusCode == System.Net.HttpStatusCode.Forbidden)
        //                {
        //                    if (result.Content.ReadAsStringAsync().Result.IndexOf("errors") != -1)
        //                    {
        //                        JObject jobj = JObject.Parse(result.Content.ReadAsStringAsync().Result);
        //                        _context.Apiloggers.Add(new Apilogger
        //                        {
        //                            Message = jobj.SelectToken("errors[0].message").ToString(),
        //                            Statuscode = jobj.SelectToken("errors[0].code").ToString(),
        //                            Innermessage = jobj.SelectToken("errors[0].description").ToString(),
        //                            Sourceoferror = "UBPBalanceAPI"
        //                        });
        //                        _context.SaveChanges();
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            _context.Apiloggers.Add(new Apilogger
        //            {
        //                Message = ex.Message,
        //                Innermessage = ex.InnerException.Message,
        //                Statuscode = "",
        //                Sourceoferror = "AccountSummery"
        //            });
        //            _context.SaveChanges();
        //            return "";
        //        }
        //    }

        //    int _index = 0;
        //    foreach (UserSummaryResponse item in lstAccSummery)
        //    {
        //        var lstcbank = _context.CustomerBankInfos.Where(x => x.CustomerId == item.customerid && x.BankId == item.bankid && x.Username == banklist[_index].Username).ToList();
        //        _index = _index + 1;
        //        if (lstcbank.Count == 0)
        //            continue;
        //        foreach (CustomerBankInfo cbi in lstcbank)
        //        {
        //            cbi.Branch = item.branch_address;
        //            cbi.Ifsc = item.ifsc_code;
        //            cbi.AccountNo = item.account_number;
        //            cbi.Name = item.name;
        //            _context.CustomerBankInfos.Attach(cbi);
        //            _context.Entry(cbi).State = EntityState.Modified;
        //            var lstcbanksummery = _context.CustomerAccountSummeries.SingleOrDefault(x => x.Customerbankid == cbi.Customerbankid);
        //            if (lstcbanksummery != null)
        //            {
        //                lstcbanksummery.Savingbalance = item.savingbalance;
        //                lstcbanksummery.Depositbalance = item.depositbalance;
        //                _context.CustomerAccountSummeries.Attach(lstcbanksummery);
        //                _context.Entry(lstcbanksummery).State = EntityState.Modified;
        //            }
        //            else
        //            {
        //                var t = new CustomerAccountSummery
        //                {
        //                    Savingbalance = item.savingbalance,
        //                    Depositbalance = item.depositbalance,
        //                    Customerbankid = cbi.Customerbankid
        //                };
        //                _context.CustomerAccountSummeries.Add(t);
        //            }
        //            _context.SaveChanges();
        //        }
        //    }
        //    return accountsummery;
        //}
        //private string UBPSummery(CustomerProfile user, int banktypeid, string username = "")
        //{
        //    string accountsummery = string.Empty;
        //    var banklist = new List<CustomerBankInfo>();
        //    //var bankinfo = new CustomerBankInfo();
        //    if (!string.IsNullOrWhiteSpace(username))
        //        banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid && x.Username == username).ToList();
        //    else
        //        banklist = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid).ToList();
        //    if (banklist.Count == 0)
        //        //    if (!string.IsNullOrWhiteSpace(username))
        //        //    bankinfo = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid && x.Username == username).SingleOrDefault();
        //        //else
        //        //    bankinfo = _context.CustomerBankInfos.Where(x => x.CustomerId == user.Customerid && x.BankId == banktypeid).SingleOrDefault();
        //        //if (bankinfo == null)
        //        return null;
        //    List<UserSummaryResponse> lstAccSummery = new List<UserSummaryResponse>();
        //    UBPSummaryRequest usercredentials = new UBPSummaryRequest();
        //    for (int i = 0; i < banklist.Count; i++)
        //    {
        //        try
        //        {
        //            usercredentials.username = banklist[i].Username.ToString();
        //            usercredentials.password = Decrypt(banklist[i].Password);
        //            usercredentials.account_name = "Sample Sandbox Account";
        //            using (var client = new HttpClient())
        //            {
        //                var request = new HttpRequestMessage
        //                {
        //                    Method = HttpMethod.Post,
        //                    RequestUri = new Uri(_appSettings.UBP_url + "sandbox/v1/accounts"),
        //                    Content = new StringContent(JsonConvert.SerializeObject(usercredentials), Encoding.UTF8, "application/json")
        //                };
        //                client.DefaultRequestHeaders.Accept.Clear();
        //                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        //                request.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");
        //                client.DefaultRequestHeaders.Add("x-ibm-client-id", _appSettings.UBP_client_id);
        //                client.DefaultRequestHeaders.Add("x-ibm-client-secret", _appSettings.UBP_client_secret);
        //                var response = client.SendAsync(request);
        //                response.Wait();
        //                var result = response.Result;
        //                if (result.IsSuccessStatusCode)
        //                {
        //                    if (result.Content.ReadAsStringAsync().Result.IndexOf("errors") != -1)
        //                    {
        //                        JObject jobj = JObject.Parse(result.Content.ReadAsStringAsync().Result);
        //                        _context.Apiloggers.Add(new Apilogger
        //                        {
        //                            Message = jobj.SelectToken("errors[0].message").ToString(),
        //                            Statuscode = jobj.SelectToken("errors[0].code").ToString(),
        //                            Sourceoferror = "UBPAccountCreationAPI"
        //                        });
        //                        _context.SaveChanges();
        //                    }
        //                    else
        //                    {
        //                        switch ((BankNames)banktypeid)
        //                        {
        //                            case BankNames.Union_Bank_Of_Philippines:
        //                                UBP_AccountCreateResponse _ubp_accountcreateresponse = new UBP_AccountCreateResponse();
        //                                var ubp_accountcreateresponseTask = result.Content.ReadAsAsync<UBP_AccountCreateResponse>();
        //                                ubp_accountcreateresponseTask.Wait();
        //                                _ubp_accountcreateresponse = ubp_accountcreateresponseTask.Result;
        //                                lstAccSummery.Add(new UserSummaryResponse()
        //                                {
        //                                    savingbalance = Convert.ToDecimal(_ubp_accountcreateresponse.data.account.balance),
        //                                    depositbalance = 0.0M,
        //                                    name = _ubp_accountcreateresponse.data.account.account_name,
        //                                    branch_address = "",
        //                                    ifsc_code = "",
        //                                    account_number = _ubp_accountcreateresponse.data.account.account_number,
        //                                    customerid = user.Customerid,
        //                                    bankid = banktypeid
        //                                });
        //                                accountsummery = JsonConvert.SerializeObject((lstAccSummery.Count > 0 ? lstAccSummery[0] : lstAccSummery));
        //                                break;
        //                            default:
        //                                break;
        //                        }
        //                    }
        //                }
        //                else if (result.StatusCode == System.Net.HttpStatusCode.Forbidden)
        //                {
        //                    if (result.Content.ReadAsStringAsync().Result.IndexOf("errors") != -1)
        //                    {
        //                        JObject jobj = JObject.Parse(result.Content.ReadAsStringAsync().Result);
        //                        _context.Apiloggers.Add(new Apilogger
        //                        {
        //                            Message = jobj.SelectToken("errors[0].message").ToString(),
        //                            Statuscode = jobj.SelectToken("errors[0].code").ToString(),
        //                            Sourceoferror = "UBPAccountCreationAPI"
        //                        });
        //                        _context.SaveChanges();
        //                    }
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            _context.Apiloggers.Add(new Apilogger
        //            {
        //                Message = ex.Message,
        //                Innermessage = ex.InnerException.Message,
        //                Statuscode = "",
        //                Sourceoferror = "AccountSummery"
        //            });
        //            _context.SaveChanges();
        //            return "";
        //        }
        //    }

        //    int _index = 0;
        //    foreach (UserSummaryResponse item in lstAccSummery)
        //    {
        //        var lstcbank = _context.CustomerBankInfos.Where(x => x.CustomerId == item.customerid && x.BankId == item.bankid && x.Username == banklist[_index].Username).ToList();
        //        _index = _index + 1;
        //        if (lstcbank.Count == 0)
        //            continue;
        //        foreach (CustomerBankInfo cbi in lstcbank)
        //        {
        //            cbi.Branch = item.branch_address;
        //            cbi.Ifsc = item.ifsc_code;
        //            cbi.AccountNo = item.account_number;
        //            cbi.Name = item.name;
        //            _context.CustomerBankInfos.Attach(cbi);
        //            _context.Entry(cbi).State = EntityState.Modified;
        //            var lstcbanksummery = _context.CustomerAccountSummeries.SingleOrDefault(x => x.Customerbankid == cbi.Customerbankid);
        //            if (lstcbanksummery != null)
        //            {
        //                lstcbanksummery.Savingbalance = item.savingbalance;
        //                lstcbanksummery.Depositbalance = item.depositbalance;
        //                _context.CustomerAccountSummeries.Attach(lstcbanksummery);
        //                _context.Entry(lstcbanksummery).State = EntityState.Modified;
        //            }
        //            else
        //            {
        //                var t = new CustomerAccountSummery
        //                {
        //                    Savingbalance = item.savingbalance,
        //                    Depositbalance = item.depositbalance,
        //                    Customerbankid = cbi.Customerbankid
        //                };
        //                _context.CustomerAccountSummeries.Add(t);
        //            }
        //            _context.SaveChanges();
        //        }
        //    }
        //    return accountsummery;
        //}
        private string Encrypt(string password)
        {
            string EncryptionKey = "bQeThWmZq4t6w9z$C&F)J@NcRfUjXn2r";
            byte[] clearBytes = Encoding.Unicode.GetBytes(password);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    password = Convert.ToBase64String(ms.ToArray());
                }
            }
            return password;
        }
        private string Decrypt(string decpassword)
        {
            string EncryptionKey = "bQeThWmZq4t6w9z$C&F)J@NcRfUjXn2r";
            byte[] cipherBytes = Convert.FromBase64String(decpassword);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    decpassword = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return decpassword;
        }
    }
}
