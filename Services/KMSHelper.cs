﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Amazon.KeyManagementService;
using Amazon.KeyManagementService.Model;
using Amazon.Runtime;
using log4net;
namespace AWSBankClientAPI.Services
{
    public class KMSHelper
    {
        private AmazonKeyManagementServiceClient client;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(KMSHelper));
        private readonly string accessKey, secretKey, serviceUrl;
        public KMSHelper() { }
        public KMSHelper(string accessKey, string secretKey, string serviceUrl)
        {
            this.accessKey = accessKey;
            this.secretKey = secretKey;
            this.serviceUrl = serviceUrl;
            client = GetClient();
        }

        private AmazonKeyManagementServiceClient GetClient()
        {
            if (client == null)
            {
                try
                {
                    // DynamoDB config object
                    AmazonKeyManagementServiceConfig clientConfig = new AmazonKeyManagementServiceConfig
                    {
                        // Set the endpoint URL
                        ServiceURL = serviceUrl
                    };
                    client = new AmazonKeyManagementServiceClient(accessKey, secretKey, clientConfig);
                }
                catch (AmazonKeyManagementServiceException ex)
                { _logger.Error($"Error (AmazonKeyManagementServiceException) creating KMS client", ex); }
                catch (AmazonServiceException ex)
                { _logger.Error($"Error (AmazonServiceException) creating KMS client", ex); }
                catch (Exception ex)
                { _logger.Error($"Error creating KMS client", ex); }
            }
            return client;
        }

        /// <summary>
            /// Generates new data key under the master key
            /// </summary>
            /// <param name="masterKeyId">Master key</param>
            /// <returns></returns>
        private async Task<MemoryStream> GenerateDataKey(string masterKeyId)
        {
            using (var kmsClient = GetClient())
            {
                var result = await kmsClient.GenerateDataKeyAsync(new GenerateDataKeyRequest
                {
                    KeyId = masterKeyId,
                    KeySpec = DataKeySpec.AES_256
                });

                return result.CiphertextBlob;
            }
        }

        /// <summary>
            /// Decrypts the data key using the master key
            /// </summary>
            /// <param name="ciphertext">Stream to decrypt</param>
            /// <returns></returns>
        public async Task<MemoryStream> DecryptDataKey(MemoryStream ciphertext, string masterKeyId)
        {
            using (var kmsClient = GetClient())
            {
                var decryptionResponse = await kmsClient.DecryptAsync(new DecryptRequest
                {
                    CiphertextBlob = ciphertext,
                    KeyId = masterKeyId
                });

                return decryptionResponse.Plaintext;
            }
        }

        /// <summary>
            /// Encrypts the data key using the master key
            /// </summary>
            /// <param name="masterKeyId">Master key arn</param>
            /// <param name="plaintext">Plain data key</param>
            /// <returns></returns>
        public async Task<MemoryStream> EncryptDataKey(string masterKeyId, MemoryStream plaintext)
        {
            using (var kmsClient = new AmazonKeyManagementServiceClient())
            {
                var encryptionResponse = await kmsClient.EncryptAsync(new EncryptRequest
                {
                    KeyId = masterKeyId,
                    Plaintext = plaintext
                });

                return encryptionResponse.CiphertextBlob;
                //return Convert.ToBase64String(encryptionResponse.CiphertextBlob.ToArray());

            }
        }
        public async Task<byte[]> EncryptDataWithKeybytes(byte[] textToDecrypt, string masterKeyId)
        {
            var kmsClient = new AmazonKeyManagementServiceClient();
            //var kmsClient = new AmazonKeyManagementServiceClient(accessKey, secretKey, serviceUrl);

            using (var algorithm = Aes.Create())
            {
                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // Generates the data key under the master key
                    var dataKey = await kmsClient.GenerateDataKeyAsync(new GenerateDataKeyRequest
                    {
                        KeyId = masterKeyId,
                        KeySpec = DataKeySpec.AES_128
                    });

                    msEncrypt.WriteByte((byte)dataKey.CiphertextBlob.Length);
                    dataKey.CiphertextBlob.CopyTo(msEncrypt);
                    algorithm.Key = dataKey.Plaintext.ToArray();

                    // Writing algorithm.IV in output stream for decryption purpose.
                    msEncrypt.Write(algorithm.IV, 0, algorithm.IV.Length);

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform encryptor = algorithm.CreateEncryptor(algorithm.Key, algorithm.IV);

                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (MemoryStream input = new MemoryStream(textToDecrypt))
                        {
                            input.CopyTo(csEncrypt);
                            csEncrypt.FlushFinalBlock();
                        }
                        return msEncrypt.ToArray();
                    }
                }
            }
        }
        public async Task<byte[]> DecryptDataWithKeybytes(byte[] textToDecrypt, string masterKeyId)
        {
            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(textToDecrypt))
            {
                var length = msDecrypt.ReadByte();
                var buffer = new byte[length];
                msDecrypt.Read(buffer, 0, length);

                // Decrypt the datakey
                MemoryStream dataKeyCipher = await DecryptDataKey(new MemoryStream(buffer), masterKeyId);

                using (var algorithm = Aes.Create())
                {
                    algorithm.Key = dataKeyCipher.ToArray();

                    var iv = algorithm.IV;
                    msDecrypt.Read(iv, 0, iv.Length);
                    algorithm.IV = iv;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = algorithm.CreateDecryptor(algorithm.Key, algorithm.IV);

                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (MemoryStream srDecrypt = new MemoryStream())
                        {
                            csDecrypt.CopyTo(srDecrypt);

                            //Write all data to the stream.
                            return srDecrypt.ToArray();
                        }
                    }
                }
            }
        }
        public async Task<MemoryStream> EncryptDataWithKey(byte[] textToDecrypt, string masterKeyId)
        {
            try
            {
                using (var kmsClient = new AmazonKeyManagementServiceClient())
                {
                    var response = await kmsClient.EncryptAsync(new EncryptRequest
                    {
                        Plaintext = new MemoryStream(textToDecrypt), // The encrypted data (ciphertext).
                        KeyId = masterKeyId // A key identifier for the KMS key to use to decrypt the data.
                    });

                    string keyId = response.KeyId; // The Amazon Resource Name (ARN) of the KMS key that was used to decrypt the data.
                    return response.CiphertextBlob; // The decrypted (plaintext) data.
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }
            return new MemoryStream();
        }
        public async Task<MemoryStream> DecryptDataWithKey(MemoryStream textToDecrypt, string masterKeyId)
        {
            //if (string.IsNullOrEmpty(EncryptedText))
            //    return new MemoryStream();
            try
            {
                using (var kmsClient = new AmazonKeyManagementServiceClient())
                {
                    var response = await kmsClient.DecryptAsync(new DecryptRequest
                    {
                        CiphertextBlob = textToDecrypt, // The encrypted data (ciphertext).
                        KeyId = masterKeyId // A key identifier for the KMS key to use to decrypt the data.
                    });

                    string keyId = response.KeyId; // The Amazon Resource Name (ARN) of the KMS key that was used to decrypt the data.
                    return response.Plaintext; // The decrypted (plaintext) data.
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
            }
            return new MemoryStream();
        }

        public string EncryptText(object textToEncrypt, string masterKeyId)
        {
            if (textToEncrypt == null)
                return "";
            try
            {
                var client = new AmazonKeyManagementServiceClient(accessKey, secretKey, serviceUrl);
                var encryptRequest = new EncryptRequest();
                encryptRequest.KeyId = masterKeyId;
                //var textbytes = Encoding.ASCII.GetBytes(textToEncrypt);
                var textbytes = ConvertObjectToBytes(textToEncrypt);
                encryptRequest.Plaintext = new MemoryStream(textbytes, 0, textbytes.Length);
                var response = client.EncryptAsync(encryptRequest);
                if (response != null)
                {

                    return Convert.ToBase64String(response.Result.CiphertextBlob.ToArray());
                }

            }
            catch (Exception)
            { }
            return "";
        }
        public MemoryStream DecryptText(string EncryptedText)
        {
            if (string.IsNullOrEmpty(EncryptedText))
                return new MemoryStream();
            try
            {
                var client = new AmazonKeyManagementServiceClient(accessKey, secretKey, serviceUrl);
                var decryptRequest = new DecryptRequest();
                var fromBase64Bytes = Convert.FromBase64String(EncryptedText);
                decryptRequest.CiphertextBlob = new MemoryStream(fromBase64Bytes, 0, fromBase64Bytes.Length);
                var response = client.DecryptAsync(decryptRequest);
                if (response != null)
                {
                    return response.Result.Plaintext;
                    //return Encoding.UTF8.GetString(response.Result.Plaintext.ToArray());
                }
            }
            catch (Exception)
            {
            }
            return new MemoryStream();
        }
        private static byte[] ConvertObjectToBytes(object cred)
        {
            byte[] bytes;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, cred);
                bytes = ms.ToArray();
            }

            return bytes;
        }


        /// <summary>
            /// Encrypts the text using the master key
            /// </summary>
            /// <param name="textToEncrypt">Text to encrypt</param>
            /// <param name="masterKeyId">Master key arn</param>
            /// <returns></returns>
        public async Task<string> Encrypt(byte[] textToEncrypt, string masterKeyId)
        {
            //var kmsClient = new AmazonKeyManagementServiceClient(accessKey, secretKey, serviceUrl);
            var kmsClient = new AmazonKeyManagementServiceClient();

            using (var algorithm = Aes.Create())
            {
                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    // Generates the data key under the master key
                    var dataKey = await kmsClient.GenerateDataKeyAsync(new GenerateDataKeyRequest
                    {
                        KeyId = masterKeyId,
                        KeySpec = DataKeySpec.AES_256
                    });

                    msEncrypt.WriteByte((byte)dataKey.CiphertextBlob.Length);
                    dataKey.CiphertextBlob.CopyTo(msEncrypt);
                    algorithm.Key = dataKey.Plaintext.ToArray();

                    // Writing algorithm.IV in output stream for decryption purpose.
                    msEncrypt.Write(algorithm.IV, 0, algorithm.IV.Length);

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform encryptor = algorithm.CreateEncryptor(algorithm.Key, algorithm.IV);

                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (MemoryStream input = new MemoryStream(textToEncrypt))
                        {
                            input.CopyTo(csEncrypt);
                            csEncrypt.FlushFinalBlock();
                        }
                        return Convert.ToBase64String(msEncrypt.ToArray());
                    }
                }
            }
        }

        /// <summary>
            /// Decrypts the text encrypted with the master key
            /// </summary>
            /// <param name="textToDecrypt">Encrypted text to decrypt</param>
            /// <returns></returns>
        public async Task<string> Decrypt(byte[] textToDecrypt, string masterKeyId)
        {
            // Create the streams used for decryption.
            using (MemoryStream msDecrypt = new MemoryStream(textToDecrypt))
            {
                var length = msDecrypt.ReadByte();
                var buffer = new byte[length];
                msDecrypt.Read(buffer, 0, length);

                // Decrypt the datakey
                MemoryStream dataKeyCipher = await DecryptDataKey(new MemoryStream(buffer), masterKeyId);

                using (var algorithm = Aes.Create())
                {
                    algorithm.Key = dataKeyCipher.ToArray();

                    var iv = algorithm.IV;
                    msDecrypt.Read(iv, 0, iv.Length);
                    algorithm.IV = iv;

                    // Create a decrytor to perform the stream transform.
                    ICryptoTransform decryptor = algorithm.CreateDecryptor(algorithm.Key, algorithm.IV);

                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (MemoryStream srDecrypt = new MemoryStream())
                        {
                            csDecrypt.CopyTo(srDecrypt);

                            //Write all data to the stream.
                            return Encoding.ASCII.GetString(srDecrypt.ToArray());
                        }
                    }
                }
            }
        }
    }
}
