﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AWSBankClientAPI.Models;
using AWSBankClientAPI.Services;
using Microsoft.AspNetCore.Cors;
using System.Collections.Generic;

namespace AWSBankClientAPI.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    //[EnableCors("AllowOrigin")]
    [ApiController]
    //[Route("[controller]")]
    public class BankingController : ControllerBase
    {
        private IBankMethodsService _bankmethodsservice;
        public BankingController(IBankMethodsService bankmethodsservice)
        {
            _bankmethodsservice = bankmethodsservice;
        }
        [HttpGet]
        [Route("[controller]/summary")]
        public IActionResult Summary(string email, int banktypeid)
        {
            var user = _bankmethodsservice.Getsummary(email, banktypeid);
            if (user == null)
                return BadRequest(new { message = "Invalid data provided!" });
            return Ok(user);
        }
        [HttpGet]
        [Route("[controller]/accountlistsummary")]
        public IActionResult accountlistsummary(string email)
        {
            var user = _bankmethodsservice.GetAccountListsummary(email);
            if (user == null)
                return BadRequest(new { message = "Invalid data provided!" });
            return Ok(user);
        }
        [HttpGet]
        [Route("[controller]/updateaccountsummery")]
        public IActionResult UpdateAccountSummery(string email)
        {
            var user = _bankmethodsservice.UpdateAccountSummery(email);
            if (!user)
                return BadRequest(new { message = "Invalid data provided!" });
            return Ok(user);
        }
        [HttpGet]
        [Route("[controller]/updatebankaccountsummery")]
        public IActionResult UpdateBankAccountSummery(int customerid, string AccountNo, int BankId)
        {
            var user = _bankmethodsservice.UpdateBankAccountSummery(customerid, AccountNo, BankId);
            if (!user)
                return BadRequest(new { message = "Invalid data provided!" });
            return Ok(user);
        }
        [HttpGet]
        [Route("[controller]/refreshalltransactions")]
        public IActionResult refreshalltransactions(string email, string from_date, string to_date)
        {
            var Transactuser = _bankmethodsservice.GetTransactions(email, from_date, to_date);
            if (Transactuser == null)
                return BadRequest(new { message = "Invalid data provided!" });
            var user = _bankmethodsservice.GetAllTransactions(email, from_date, to_date, 0);
            if (user == null)
                return BadRequest(new { message = "Invalid data provided!" });
            return Ok(user);
        }
        [HttpGet]
        [Route("[controller]/refreshspecifictransactions")]
        public IActionResult refreshspecifictransactions(string email, int banktypeid, string from_date, string to_date)
        {
            var spectransact = _bankmethodsservice.GetSpecTransactions(email, banktypeid, from_date, to_date);
            if (spectransact == null)
                return BadRequest(new { message = "Invalid data provided!" });
            var user = _bankmethodsservice.GetAllTransactions(email, from_date, to_date, banktypeid);
            return Ok(user);
        }

        [HttpGet]
        [Route("[controller]/getalltransactions")]
        public IActionResult Getalltransactions(string email, string from_date, string to_date, int bankid)
        {
            var user = _bankmethodsservice.GetAllTransactions(email, from_date, to_date, bankid);
            if (user == null)
                return BadRequest(new { message = "Invalid data provided!" });
            return Ok(user);
        }


        [HttpGet]
        [Route("[controller]/dashboard")]
        public IActionResult dashboard(string email, string from_date, string to_date, int bankid)
        {
            var user = _bankmethodsservice.GetTransactionsbycategory(email, from_date, to_date, bankid);
            if (user == null)
                return BadRequest(new { message = "Invalid data provided!" });
            return Ok(user);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankinfo"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[controller]/savebank")]
        public IActionResult saveBank(BankAccount bankinfo)
        {
            List<Secureddata> customerBank = (List<Secureddata>)_bankmethodsservice.getBankDetails(bankinfo).Result;
            if (customerBank.Count == 0)
            {
                var user = _bankmethodsservice.AddBank(bankinfo);
                if (user.Result > 0)
                {
                    if (bankinfo.bankid != 2)
                    {
                        var _accSummery = _bankmethodsservice.Getupdatedsummery(bankinfo.customerid);
                        // var _accTransactions = _bankmethodsservice.InitTransactiondata(bankinfo.customerid, bankinfo.bankid);
                        return Ok(_accSummery);
                    }
                    else
                        return Ok();
                }
                else
                    return BadRequest(new { message = "Bank details already exist!" });
            }
            else
                return BadRequest(new { message = "Bank details already exist!" });
        }

        [HttpPost]
        [Route("[controller]/updateotp")]
        public IActionResult Updateotp(UpdateOtp values)
        {
            var otpstatus = _bankmethodsservice.Updateotp(values.customerid, values.bankid, values.username, values.otp);
            if (otpstatus > 0)
            {
                return Ok(otpstatus);
            }
            else
                return BadRequest(new { message = "Error in updating OTP!" });
        }

        [HttpDelete]
        [Route("[controller]/deletebank")]
        public IActionResult deletebank(int customerId, int bankId)
        {
            var respid = _bankmethodsservice.DeleteBank(customerId, bankId);
            if (respid == 0)
                return BadRequest(new { message = "Invalid data provided!" });
            return Ok(respid);
        }
    }
}
