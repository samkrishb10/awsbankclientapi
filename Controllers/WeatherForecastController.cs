﻿using AWSBankClientAPI.Models;
using AWSBankClientAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace AWSBankClientAPI.Controllers
{
    [EnableCors("AllowOrigin")]
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        [Serializable]
        public class myClass
        {
            public string username { get; set; }
            public string password { get; set; }
        }
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async System.Threading.Tasks.Task<IEnumerable<WeatherForecast>> GetAsync()
        {
            try
            {

                //KMSHelper kMSHelper = new KMSHelper("ASIAUXSN37QYDYZJAQME", "L1XI2VG+vXXVgurf1vnq6ISK3hoYTjWc89SNxrHJ", "FwoGZXIvYXdzEJL//////////wEaDJ5Y46QHQ8M0odPwPiKCAfw6M4vqCSt2ECIe2Cs9U/YhSuwyK5vjvb4t38/Z3QqPVnqWxzN35LOp0Dr+OQjpxVtR7vngdVJJq/FMs0l67l1ORFkTnoC+DHCIG7aU6zl0zRe60bcM6uW9qI3haU6l8HMgTH93S51EgEDvyGiEpazJNF4ZPsVrs7cBrG1bCyZ9QwAogNfLiwYyKGx1sQ7Jm3FA3HO63EcjYU2sjK+IPCt8W9YEVUUcNjzksHZTpN06L0E=");
                //KMSHelper kMSHelper = new KMSHelper("AKIAUXSN37QYMPDQXZPE", "OE9HhglSY1OeUtdP2+kXIzm4e5U39H8bWK78E1s+", "https://sts.amazonaws.com");
                KMSHelper kMSHelper = new KMSHelper();
                myClass cred = new myClass
                {
                    username = "888885573",
                    password = "Cfoa6sUizNunFrpTVEdBY6TqPdgCjrNUYx4lI/i5uug="
                };
                byte[] bytes;
                string masterKey = "arn:aws:kms:ap-south-1:325507087408:key/1e659b53-53c4-4340-b345-03b4aece0628";
                bytes = ConvertObjToBytes(cred);
                MemoryStream encryptedText = await kMSHelper.EncryptDataWithKey(bytes, masterKey);
                byte[] encryptbytes = encryptedText.ToArray();
                MemoryStream memoryStream = await kMSHelper.DecryptDataWithKey(new MemoryStream(encryptbytes), masterKey);
                myClass newDog = (myClass)DeserializeFromStream(memoryStream);

                var rng = new Random();
                return Enumerable.Range(1, 5).Select(index => new WeatherForecast
                {
                    Date = DateTime.Now.AddDays(index),
                    TemperatureC = rng.Next(-20, 55),
                    Summary = Summaries[rng.Next(Summaries.Length)]
                })
                .ToArray();

                //string plainText = "";
                //MemoryStream ms = new MemoryStream();
                // bytes = ConvertStringToBytes(cred);
                //bytes = Encoding.ASCII.GetBytes("Test");
                //string securestring = "Test";
                //ms = new MemoryStream(bytes);
                //bytes = await kMSHelper.EncryptDataWithKeybytes(bytes, masterKey);
                //bytes = Encoding.ASCII.GetBytes(_encryptedText);
                //MemoryStream memoryStream = await kMSHelper.DecryptDataWithKey(_encryptedText, masterKey);
                //bytes = await kMSHelper.DecryptDataWithKeybytes(bytes, masterKey);
                //Encryptedbytes = kMSHelper.EncryptText(cred, "1e659b53-53c4-4340-b345-03b4aece0628");
                //bytes = Encoding.ASCII.GetBytes(_decryptedText);
                //ms = new MemoryStream(bytes);

                //ms = kMSHelper.DecryptText(Encryptedbytes);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                throw;
            }
        }
      
        private static byte[] ConvertObjToBytes(object cred)
        {
            byte[] bytes;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, cred);
                bytes = ms.ToArray();
            }

            return bytes;
        }
        public static object DeserializeFromStream(MemoryStream stream)
        {
            IFormatter formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            object o = formatter.Deserialize(stream);
            return o;
        }

        [HttpPost]
        public string postset([FromBody] MailValidation CP)
        {
            return "Welcome " + CP.email + " post method";
        }
    }
}
