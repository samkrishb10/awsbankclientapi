﻿using AWSBankClientAPI.Models;
using AWSBankClientAPI.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace AWSBankClientAPI.Controllers
{
    //[EnableCors("AllowOrigin")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private IAuthenticateService _authenticateService;
        public AuthenticationController(IAuthenticateService authenticateService)
        {
            _authenticateService = authenticateService;
        }
        //[HttpPost]
        //[Route("[controller]")]
        //public IActionResult Post([FromBody] CustomerProfile model)
        //{
        //    var user = _authenticateService.Authenticate(model.Email);
        //    if (user == null)
        //        return BadRequest(new { message = "Username cannot be blank" });
        //    return Ok(user);
        //}
        /// <summary>
        /// Email alone required
        /// </summary>
        /// <param name="CP"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("[controller]/register")]
        public IActionResult Register([FromBody] MailValidation CP)
        {
            var user = _authenticateService.MailValidation(CP.email);
            if (user == -1)
                return BadRequest(new { message = "Email does not exist!" });
            else if (user == 0)
                return BadRequest(new { message = "Error Sending One Time Registration Password Email!" });
            return Ok(user);
        }
        [HttpPost]
        [Route("[controller]/verify")]
        public IActionResult verify([FromBody] VerifyOTPEmail CP)
        {
            var user = _authenticateService.VerifyOTPEmail(CP.email, CP.otp);
            if (user == null)
                return BadRequest(new { message = "OTP is incorrect!" });

            return Ok(user);
        }
        [HttpPost]
        [Route("[controller]/getaccessinfo")]
        public IActionResult getaccessinfo([FromBody] GenerateToken model)
        {
            //var user = _authenticateService.GenerateJWT(model.email, model.apikey, model.secretkey);
            var user = _authenticateService.GenerateJWT(model.email, model.apikey);
            //if (user == null)
            //    return "Invalid data provided!";
            if (user == null)
                return BadRequest(new { message = "Invalid Details!" });
            return Ok(user);
        }
        //[HttpPost]
        //[Route("[controller]/update")]
        //public IActionResult update([FromBody] CustomerProfile model)
        //{
        //    var user = _authenticateService.UpdateInfo(model);
        //    if (user == null)
        //        return BadRequest(new { message = "Invalid data provided!" });
        //    return Ok(user);
        //}
    }
}
