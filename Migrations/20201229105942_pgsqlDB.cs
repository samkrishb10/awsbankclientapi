﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AWSBankClientAPI.Migrations
{
    public partial class pgsqlDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "banks_info",
                columns: table => new
                {
                    bank_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bank_name = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("banks_info_pkey", x => x.bank_id);
                });

            migrationBuilder.CreateTable(
                name: "customer_profile",
                columns: table => new
                {
                    customerid = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    age = table.Column<int>(type: "integer", nullable: true),
                    address = table.Column<string>(type: "text", nullable: true),
                    marital_status = table.Column<string>(type: "text", nullable: true),
                    mobile = table.Column<string>(type: "text", nullable: true),
                    email = table.Column<string>(type: "text", nullable: true),
                    apikey = table.Column<string>(type: "text", nullable: true),
                    secretkey = table.Column<string>(type: "text", nullable: true),
                    otp = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("customer_profile_pkey", x => x.customerid);
                });

            migrationBuilder.CreateTable(
                name: "user_profile",
                columns: table => new
                {
                    user_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: true),
                    mobile = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    email = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    age = table.Column<int>(type: "integer", nullable: true),
                    marital_status = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    address = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("user_profile_pkey", x => x.user_id);
                });

            migrationBuilder.CreateTable(
                name: "bank_xpath",
                columns: table => new
                {
                    bank_id = table.Column<int>(type: "integer", nullable: true),
                    bank_url = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    userid_xpath = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    password_xpath = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    captcha_xpath = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    login_xpath = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.ForeignKey(
                        name: "bank_xpath_bank_id_fkey",
                        column: x => x.bank_id,
                        principalTable: "banks_info",
                        principalColumn: "bank_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "account_statements",
                columns: table => new
                {
                    hash_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bank_id = table.Column<int>(type: "integer", nullable: true),
                    user_id = table.Column<int>(type: "integer", nullable: true),
                    date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    description = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    credit_amount = table.Column<double>(type: "double precision", nullable: true),
                    debit_amount = table.Column<double>(type: "double precision", nullable: true),
                    all_attributes = table.Column<string>(type: "json", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("account_statements_pkey", x => x.hash_id);
                    table.ForeignKey(
                        name: "account_statements_bank_id_fkey",
                        column: x => x.bank_id,
                        principalTable: "banks_info",
                        principalColumn: "bank_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "account_statements_user_id_fkey",
                        column: x => x.user_id,
                        principalTable: "user_profile",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "account_summery",
                columns: table => new
                {
                    bank_id = table.Column<int>(type: "integer", nullable: false),
                    user_id = table.Column<int>(type: "integer", nullable: false),
                    account_type = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    account_no = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    balance = table.Column<double>(type: "double precision", nullable: true),
                    deposit_type = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: true),
                    date_of_deposit = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    deposit_amount = table.Column<double>(type: "double precision", nullable: true),
                    maturity_date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    maturity_amount = table.Column<double>(type: "double precision", nullable: true),
                    interest_rate = table.Column<float>(type: "real", nullable: true),
                    nomination_registered = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    auto_renewal = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    auto_closure = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: true),
                    all_attributes = table.Column<string>(type: "json", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("account_summery_pkey", x => new { x.bank_id, x.user_id, x.account_type, x.account_no });
                    table.ForeignKey(
                        name: "account_summery_bank_id_fkey",
                        column: x => x.bank_id,
                        principalTable: "banks_info",
                        principalColumn: "bank_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "account_summery_user_id_fkey",
                        column: x => x.user_id,
                        principalTable: "user_profile",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "account_transactions",
                columns: table => new
                {
                    hash_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bank_id = table.Column<int>(type: "integer", nullable: true),
                    user_id = table.Column<int>(type: "integer", nullable: true),
                    date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    description = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    reference = table.Column<string>(type: "character varying(50)", maxLength: 50, nullable: true),
                    debit_amount = table.Column<double>(type: "double precision", nullable: true),
                    credit_amount = table.Column<double>(type: "double precision", nullable: true),
                    closing_balance = table.Column<double>(type: "double precision", nullable: true),
                    all_attributes = table.Column<string>(type: "json", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("account_transactions_pkey", x => x.hash_id);
                    table.ForeignKey(
                        name: "account_transactions_bank_id_fkey",
                        column: x => x.bank_id,
                        principalTable: "banks_info",
                        principalColumn: "bank_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "account_transactions_user_id_fkey",
                        column: x => x.user_id,
                        principalTable: "user_profile",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customer_bank_info",
                columns: table => new
                {
                    bank_id = table.Column<int>(type: "integer", nullable: false),
                    user_id = table.Column<int>(type: "integer", nullable: false),
                    account_type = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    account_no = table.Column<string>(type: "character varying(25)", maxLength: 25, nullable: false),
                    mobile = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true),
                    customer_id = table.Column<string>(type: "character varying(30)", maxLength: 30, nullable: true),
                    password = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("customer_bank_info_pkey", x => new { x.bank_id, x.user_id, x.account_type, x.account_no });
                    table.ForeignKey(
                        name: "customer_bank_info_bank_id_fkey",
                        column: x => x.bank_id,
                        principalTable: "banks_info",
                        principalColumn: "bank_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "customer_bank_info_user_id_fkey",
                        column: x => x.user_id,
                        principalTable: "user_profile",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "deposit_statements",
                columns: table => new
                {
                    hash_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bank_id = table.Column<int>(type: "integer", nullable: true),
                    user_id = table.Column<int>(type: "integer", nullable: true),
                    date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    description = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    credit_amount = table.Column<double>(type: "double precision", nullable: true),
                    debit_amount = table.Column<double>(type: "double precision", nullable: true),
                    all_attributes = table.Column<string>(type: "json", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("deposit_statements_pkey", x => x.hash_id);
                    table.ForeignKey(
                        name: "deposit_statements_bank_id_fkey",
                        column: x => x.bank_id,
                        principalTable: "banks_info",
                        principalColumn: "bank_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "deposit_statements_user_id_fkey",
                        column: x => x.user_id,
                        principalTable: "user_profile",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "deposit_transactions",
                columns: table => new
                {
                    hash_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bank_id = table.Column<int>(type: "integer", nullable: true),
                    user_id = table.Column<int>(type: "integer", nullable: true),
                    date = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    description = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: true),
                    credit_amount = table.Column<double>(type: "double precision", nullable: true),
                    closing_balance = table.Column<double>(type: "double precision", nullable: true),
                    all_attributes = table.Column<string>(type: "json", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("deposit_transactions_pkey", x => x.hash_id);
                    table.ForeignKey(
                        name: "deposit_transactions_bank_id_fkey",
                        column: x => x.bank_id,
                        principalTable: "banks_info",
                        principalColumn: "bank_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "deposit_transactions_user_id_fkey",
                        column: x => x.user_id,
                        principalTable: "user_profile",
                        principalColumn: "user_id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_account_statements_bank_id",
                table: "account_statements",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "IX_account_statements_user_id",
                table: "account_statements",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_account_summery_user_id",
                table: "account_summery",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_account_transactions_bank_id",
                table: "account_transactions",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "IX_account_transactions_user_id",
                table: "account_transactions",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_bank_xpath_bank_id",
                table: "bank_xpath",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "IX_customer_bank_info_user_id",
                table: "customer_bank_info",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_deposit_statements_bank_id",
                table: "deposit_statements",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "IX_deposit_statements_user_id",
                table: "deposit_statements",
                column: "user_id");

            migrationBuilder.CreateIndex(
                name: "IX_deposit_transactions_bank_id",
                table: "deposit_transactions",
                column: "bank_id");

            migrationBuilder.CreateIndex(
                name: "IX_deposit_transactions_user_id",
                table: "deposit_transactions",
                column: "user_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "account_statements");

            migrationBuilder.DropTable(
                name: "account_summery");

            migrationBuilder.DropTable(
                name: "account_transactions");

            migrationBuilder.DropTable(
                name: "bank_xpath");

            migrationBuilder.DropTable(
                name: "customer_bank_info");

            migrationBuilder.DropTable(
                name: "customer_profile");

            migrationBuilder.DropTable(
                name: "deposit_statements");

            migrationBuilder.DropTable(
                name: "deposit_transactions");

            migrationBuilder.DropTable(
                name: "banks_info");

            migrationBuilder.DropTable(
                name: "user_profile");
        }
    }
}
