﻿// <auto-generated />
using System;
using AWSBankClientAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace AWSBankClientAPI.Migrations
{
    [DbContext(typeof(AWSBANKAPIContext))]
    partial class AWSBANKAPIContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .UseIdentityByDefaultColumns()
                .HasAnnotation("Relational:MaxIdentifierLength", 63)
                .HasAnnotation("ProductVersion", "5.0.0");

            modelBuilder.Entity("AWSBankClientAPI.Models.AccountStatement", b =>
                {
                    b.Property<string>("HashId")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("hash_id");

                    b.Property<string>("AllAttributes")
                        .HasColumnType("json")
                        .HasColumnName("all_attributes");

                    b.Property<int?>("BankId")
                        .HasColumnType("integer")
                        .HasColumnName("bank_id");

                    b.Property<double?>("CreditAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("credit_amount");

                    b.Property<DateTime?>("Date")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("date");

                    b.Property<double?>("DebitAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("debit_amount");

                    b.Property<string>("Description")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("description");

                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .UseIdentityByDefaultColumn();

                    b.Property<int?>("UserId")
                        .HasColumnType("integer")
                        .HasColumnName("user_id");

                    b.HasKey("HashId")
                        .HasName("account_statements_pkey");

                    b.HasIndex("BankId");

                    b.HasIndex("UserId");

                    b.ToTable("account_statements");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.AccountSummery", b =>
                {
                    b.Property<int>("BankId")
                        .HasColumnType("integer")
                        .HasColumnName("bank_id");

                    b.Property<int>("UserId")
                        .HasColumnType("integer")
                        .HasColumnName("user_id");

                    b.Property<string>("AccountType")
                        .HasMaxLength(20)
                        .HasColumnType("character varying(20)")
                        .HasColumnName("account_type");

                    b.Property<string>("AccountNo")
                        .HasMaxLength(25)
                        .HasColumnType("character varying(25)")
                        .HasColumnName("account_no");

                    b.Property<string>("AllAttributes")
                        .HasColumnType("json")
                        .HasColumnName("all_attributes");

                    b.Property<string>("AutoClosure")
                        .HasMaxLength(10)
                        .HasColumnType("character varying(10)")
                        .HasColumnName("auto_closure");

                    b.Property<string>("AutoRenewal")
                        .HasMaxLength(10)
                        .HasColumnType("character varying(10)")
                        .HasColumnName("auto_renewal");

                    b.Property<double?>("Balance")
                        .HasColumnType("double precision")
                        .HasColumnName("balance");

                    b.Property<DateTime?>("DateOfDeposit")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("date_of_deposit");

                    b.Property<double?>("DepositAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("deposit_amount");

                    b.Property<string>("DepositType")
                        .HasMaxLength(40)
                        .HasColumnType("character varying(40)")
                        .HasColumnName("deposit_type");

                    b.Property<float?>("InterestRate")
                        .HasColumnType("real")
                        .HasColumnName("interest_rate");

                    b.Property<double?>("MaturityAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("maturity_amount");

                    b.Property<DateTime?>("MaturityDate")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("maturity_date");

                    b.Property<string>("NominationRegistered")
                        .HasMaxLength(10)
                        .HasColumnType("character varying(10)")
                        .HasColumnName("nomination_registered");

                    b.HasKey("BankId", "UserId", "AccountType", "AccountNo")
                        .HasName("account_summery_pkey");

                    b.HasIndex("UserId");

                    b.ToTable("account_summery");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.AccountTransaction", b =>
                {
                    b.Property<string>("HashId")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("hash_id");

                    b.Property<string>("AllAttributes")
                        .HasColumnType("json")
                        .HasColumnName("all_attributes");

                    b.Property<int?>("BankId")
                        .HasColumnType("integer")
                        .HasColumnName("bank_id");

                    b.Property<double?>("ClosingBalance")
                        .HasColumnType("double precision")
                        .HasColumnName("closing_balance");

                    b.Property<double?>("CreditAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("credit_amount");

                    b.Property<DateTime?>("Date")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("date");

                    b.Property<double?>("DebitAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("debit_amount");

                    b.Property<string>("Description")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("description");

                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Reference")
                        .HasMaxLength(50)
                        .HasColumnType("character varying(50)")
                        .HasColumnName("reference");

                    b.Property<int?>("UserId")
                        .HasColumnType("integer")
                        .HasColumnName("user_id");

                    b.HasKey("HashId")
                        .HasName("account_transactions_pkey");

                    b.HasIndex("BankId");

                    b.HasIndex("UserId");

                    b.ToTable("account_transactions");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.BankXpath", b =>
                {
                    b.Property<int?>("BankId")
                        .HasColumnType("integer")
                        .HasColumnName("bank_id");

                    b.Property<string>("BankUrl")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("bank_url");

                    b.Property<string>("CaptchaXpath")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("captcha_xpath");

                    b.Property<string>("LoginXpath")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("login_xpath");

                    b.Property<string>("PasswordXpath")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("password_xpath");

                    b.Property<string>("UseridXpath")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("userid_xpath");

                    b.HasIndex("BankId");

                    b.ToTable("bank_xpath");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.BanksInfo", b =>
                {
                    b.Property<int>("BankId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("bank_id")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("BankName")
                        .HasMaxLength(20)
                        .HasColumnType("character varying(20)")
                        .HasColumnName("bank_name");

                    b.HasKey("BankId")
                        .HasName("banks_info_pkey");

                    b.ToTable("banks_info");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.CustomerBankInfo", b =>
                {
                    b.Property<int>("BankId")
                        .HasColumnType("integer")
                        .HasColumnName("bank_id");

                    b.Property<int>("UserId")
                        .HasColumnType("integer")
                        .HasColumnName("user_id");

                    b.Property<string>("AccountType")
                        .HasMaxLength(20)
                        .HasColumnType("character varying(20)")
                        .HasColumnName("account_type");

                    b.Property<string>("AccountNo")
                        .HasMaxLength(25)
                        .HasColumnType("character varying(25)")
                        .HasColumnName("account_no");

                    b.Property<string>("CustomerId")
                        .HasMaxLength(30)
                        .HasColumnType("character varying(30)")
                        .HasColumnName("customer_id");

                    b.Property<string>("Mobile")
                        .HasMaxLength(20)
                        .HasColumnType("character varying(20)")
                        .HasColumnName("mobile");

                    b.Property<string>("Password")
                        .HasMaxLength(20)
                        .HasColumnType("character varying(20)")
                        .HasColumnName("password");

                    b.HasKey("BankId", "UserId", "AccountType", "AccountNo")
                        .HasName("customer_bank_info_pkey");

                    b.HasIndex("UserId");

                    b.ToTable("customer_bank_info");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.CustomerProfile", b =>
                {
                    b.Property<long>("Customerid")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint")
                        .HasColumnName("customerid")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Address")
                        .HasColumnType("text")
                        .HasColumnName("address");

                    b.Property<int?>("Age")
                        .HasColumnType("integer")
                        .HasColumnName("age");

                    b.Property<string>("Apikey")
                        .HasColumnType("text")
                        .HasColumnName("apikey");

                    b.Property<string>("Email")
                        .HasColumnType("text")
                        .HasColumnName("email");

                    b.Property<string>("MaritalStatus")
                        .HasColumnType("text")
                        .HasColumnName("marital_status");

                    b.Property<string>("Mobile")
                        .HasColumnType("text")
                        .HasColumnName("mobile");

                    b.Property<string>("Name")
                        .HasColumnType("text")
                        .HasColumnName("name");

                    b.Property<int?>("Otp")
                        .HasColumnType("integer")
                        .HasColumnName("otp");

                    b.Property<string>("Secretkey")
                        .HasColumnType("text")
                        .HasColumnName("secretkey");

                    b.HasKey("Customerid")
                        .HasName("customer_profile_pkey");

                    b.ToTable("customer_profile");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.DepositStatement", b =>
                {
                    b.Property<string>("HashId")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("hash_id");

                    b.Property<string>("AllAttributes")
                        .HasColumnType("json")
                        .HasColumnName("all_attributes");

                    b.Property<int?>("BankId")
                        .HasColumnType("integer")
                        .HasColumnName("bank_id");

                    b.Property<double?>("CreditAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("credit_amount");

                    b.Property<DateTime?>("Date")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("date");

                    b.Property<double?>("DebitAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("debit_amount");

                    b.Property<string>("Description")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("description");

                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .UseIdentityByDefaultColumn();

                    b.Property<int?>("UserId")
                        .HasColumnType("integer")
                        .HasColumnName("user_id");

                    b.HasKey("HashId")
                        .HasName("deposit_statements_pkey");

                    b.HasIndex("BankId");

                    b.HasIndex("UserId");

                    b.ToTable("deposit_statements");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.DepositTransaction", b =>
                {
                    b.Property<string>("HashId")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("hash_id");

                    b.Property<string>("AllAttributes")
                        .HasColumnType("json")
                        .HasColumnName("all_attributes");

                    b.Property<int?>("BankId")
                        .HasColumnType("integer")
                        .HasColumnName("bank_id");

                    b.Property<double?>("ClosingBalance")
                        .HasColumnType("double precision")
                        .HasColumnName("closing_balance");

                    b.Property<double?>("CreditAmount")
                        .HasColumnType("double precision")
                        .HasColumnName("credit_amount");

                    b.Property<DateTime?>("Date")
                        .HasColumnType("timestamp without time zone")
                        .HasColumnName("date");

                    b.Property<string>("Description")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("description");

                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("id")
                        .UseIdentityByDefaultColumn();

                    b.Property<int?>("UserId")
                        .HasColumnType("integer")
                        .HasColumnName("user_id");

                    b.HasKey("HashId")
                        .HasName("deposit_transactions_pkey");

                    b.HasIndex("BankId");

                    b.HasIndex("UserId");

                    b.ToTable("deposit_transactions");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.UserProfile", b =>
                {
                    b.Property<int>("UserId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("integer")
                        .HasColumnName("user_id")
                        .UseIdentityByDefaultColumn();

                    b.Property<string>("Address")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)")
                        .HasColumnName("address");

                    b.Property<int?>("Age")
                        .HasColumnType("integer")
                        .HasColumnName("age");

                    b.Property<string>("Email")
                        .HasMaxLength(30)
                        .HasColumnType("character varying(30)")
                        .HasColumnName("email");

                    b.Property<string>("MaritalStatus")
                        .HasMaxLength(10)
                        .HasColumnType("character varying(10)")
                        .HasColumnName("marital_status");

                    b.Property<string>("Mobile")
                        .HasMaxLength(20)
                        .HasColumnType("character varying(20)")
                        .HasColumnName("mobile");

                    b.Property<string>("Name")
                        .HasMaxLength(40)
                        .HasColumnType("character varying(40)")
                        .HasColumnName("name");

                    b.HasKey("UserId")
                        .HasName("user_profile_pkey");

                    b.ToTable("user_profile");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.AccountStatement", b =>
                {
                    b.HasOne("AWSBankClientAPI.Models.BanksInfo", "Bank")
                        .WithMany("AccountStatements")
                        .HasForeignKey("BankId")
                        .HasConstraintName("account_statements_bank_id_fkey");

                    b.HasOne("AWSBankClientAPI.Models.UserProfile", "User")
                        .WithMany("AccountStatements")
                        .HasForeignKey("UserId")
                        .HasConstraintName("account_statements_user_id_fkey");

                    b.Navigation("Bank");

                    b.Navigation("User");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.AccountSummery", b =>
                {
                    b.HasOne("AWSBankClientAPI.Models.BanksInfo", "Bank")
                        .WithMany("AccountSummeries")
                        .HasForeignKey("BankId")
                        .HasConstraintName("account_summery_bank_id_fkey")
                        .IsRequired();

                    b.HasOne("AWSBankClientAPI.Models.UserProfile", "User")
                        .WithMany("AccountSummeries")
                        .HasForeignKey("UserId")
                        .HasConstraintName("account_summery_user_id_fkey")
                        .IsRequired();

                    b.Navigation("Bank");

                    b.Navigation("User");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.AccountTransaction", b =>
                {
                    b.HasOne("AWSBankClientAPI.Models.BanksInfo", "Bank")
                        .WithMany("AccountTransactions")
                        .HasForeignKey("BankId")
                        .HasConstraintName("account_transactions_bank_id_fkey");

                    b.HasOne("AWSBankClientAPI.Models.UserProfile", "User")
                        .WithMany("AccountTransactions")
                        .HasForeignKey("UserId")
                        .HasConstraintName("account_transactions_user_id_fkey");

                    b.Navigation("Bank");

                    b.Navigation("User");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.BankXpath", b =>
                {
                    b.HasOne("AWSBankClientAPI.Models.BanksInfo", "Bank")
                        .WithMany()
                        .HasForeignKey("BankId")
                        .HasConstraintName("bank_xpath_bank_id_fkey");

                    b.Navigation("Bank");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.CustomerBankInfo", b =>
                {
                    b.HasOne("AWSBankClientAPI.Models.BanksInfo", "Bank")
                        .WithMany("CustomerBankInfos")
                        .HasForeignKey("BankId")
                        .HasConstraintName("customer_bank_info_bank_id_fkey")
                        .IsRequired();

                    b.HasOne("AWSBankClientAPI.Models.UserProfile", "User")
                        .WithMany("CustomerBankInfos")
                        .HasForeignKey("UserId")
                        .HasConstraintName("customer_bank_info_user_id_fkey")
                        .IsRequired();

                    b.Navigation("Bank");

                    b.Navigation("User");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.DepositStatement", b =>
                {
                    b.HasOne("AWSBankClientAPI.Models.BanksInfo", "Bank")
                        .WithMany("DepositStatements")
                        .HasForeignKey("BankId")
                        .HasConstraintName("deposit_statements_bank_id_fkey");

                    b.HasOne("AWSBankClientAPI.Models.UserProfile", "User")
                        .WithMany("DepositStatements")
                        .HasForeignKey("UserId")
                        .HasConstraintName("deposit_statements_user_id_fkey");

                    b.Navigation("Bank");

                    b.Navigation("User");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.DepositTransaction", b =>
                {
                    b.HasOne("AWSBankClientAPI.Models.BanksInfo", "Bank")
                        .WithMany("DepositTransactions")
                        .HasForeignKey("BankId")
                        .HasConstraintName("deposit_transactions_bank_id_fkey");

                    b.HasOne("AWSBankClientAPI.Models.UserProfile", "User")
                        .WithMany("DepositTransactions")
                        .HasForeignKey("UserId")
                        .HasConstraintName("deposit_transactions_user_id_fkey");

                    b.Navigation("Bank");

                    b.Navigation("User");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.BanksInfo", b =>
                {
                    b.Navigation("AccountStatements");

                    b.Navigation("AccountSummeries");

                    b.Navigation("AccountTransactions");

                    b.Navigation("CustomerBankInfos");

                    b.Navigation("DepositStatements");

                    b.Navigation("DepositTransactions");
                });

            modelBuilder.Entity("AWSBankClientAPI.Models.UserProfile", b =>
                {
                    b.Navigation("AccountStatements");

                    b.Navigation("AccountSummeries");

                    b.Navigation("AccountTransactions");

                    b.Navigation("CustomerBankInfos");

                    b.Navigation("DepositStatements");

                    b.Navigation("DepositTransactions");
                });
#pragma warning restore 612, 618
        }
    }
}
