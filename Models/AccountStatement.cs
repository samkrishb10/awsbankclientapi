﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class AccountStatement
    {
        public int Id { get; set; }
        public string HashId { get; set; }
        public int? BankId { get; set; }
        public int? UserId { get; set; }
        public DateTime? Date { get; set; }
        public string Description { get; set; }
        public double? CreditAmount { get; set; }
        public double? DebitAmount { get; set; }
        public string AllAttributes { get; set; }

        public virtual BanksInfo Bank { get; set; }
        public virtual UserProfile User { get; set; }
    }
}
