﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class AccountSummery
    {
        public int BankId { get; set; }
        public int UserId { get; set; }
        public double? Balance { get; set; }
        public string AccountType { get; set; }
        public string AccountNo { get; set; }
        public string DepositType { get; set; }
        public DateTime? DateOfDeposit { get; set; }
        public double? DepositAmount { get; set; }
        public DateTime? MaturityDate { get; set; }
        public double? MaturityAmount { get; set; }
        public float? InterestRate { get; set; }
        public string NominationRegistered { get; set; }
        public string AutoRenewal { get; set; }
        public string AutoClosure { get; set; }
        public string AllAttributes { get; set; }

        public virtual BanksInfo Bank { get; set; }
        public virtual UserProfile User { get; set; }
    }
}
