﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class CustomerProfile
    {
        public CustomerProfile()
        {
            CustomerBankInfos = new HashSet<CustomerBankInfo>();
            CustomerBankSecureds = new HashSet<CustomerBankSecured>();
        }

        public string Name { get; set; }
        public int? Age { get; set; }
        public string Address { get; set; }
        public string MaritalStatus { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Apikey { get; set; }
        public long Customerid { get; set; }
        public int? Otp { get; set; }
        public DateTime? Expireson { get; set; }
        public bool Isverified { get; set; }
        public int? Managementid { get; set; }

        public virtual ICollection<CustomerBankInfo> CustomerBankInfos { get; set; }
        public virtual ICollection<CustomerBankSecured> CustomerBankSecureds { get; set; }
    }
}
