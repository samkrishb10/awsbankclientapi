﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class CustomerBankSecured
    {
        public int BankId { get; set; }
        public string Mobile { get; set; }
        public string AccountType { get; set; }
        public string AccountNo { get; set; }
        public byte[] Logindata { get; set; }
        public long? CustomerId { get; set; }
        public int Customerbankid { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        public string Ifsc { get; set; }
        public int? Otp { get; set; }

        public virtual BanksInfo Bank { get; set; }
        public virtual CustomerProfile Customer { get; set; }
    }
}
