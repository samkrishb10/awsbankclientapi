﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class psqlbankapiContext : DbContext
    {
        public psqlbankapiContext()
        {
        }

        public psqlbankapiContext(DbContextOptions<psqlbankapiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountStatement> AccountStatements { get; set; }
        public virtual DbSet<AccountSummery> AccountSummeries { get; set; }
        public virtual DbSet<AccountTransaction> AccountTransactions { get; set; }
        public virtual DbSet<AccountTransactionsAnalysis> AccountTransactionsAnalyses { get; set; }
        public virtual DbSet<Apilogger> Apiloggers { get; set; }
        public virtual DbSet<BankXpath> BankXpaths { get; set; }
        public virtual DbSet<BanksInfo> BanksInfos { get; set; }
        public virtual DbSet<CustomerAccountSummery> CustomerAccountSummeries { get; set; }
        public virtual DbSet<CustomerAccountTransaction> CustomerAccountTransactions { get; set; }
        public virtual DbSet<CustomerAccountTransactionAnalysis> CustomerAccountTransactionAnalyses { get; set; }
        public virtual DbSet<CustomerBankInfo> CustomerBankInfos { get; set; }
        public virtual DbSet<CustomerBankSecured> CustomerBankSecureds { get; set; }
        public virtual DbSet<CustomerProfile> CustomerProfiles { get; set; }
        public virtual DbSet<DepositStatement> DepositStatements { get; set; }
        public virtual DbSet<DepositTransaction> DepositTransactions { get; set; }
        public virtual DbSet<UserProfile> UserProfiles { get; set; }
        public virtual DbSet<UserSecurityQuestion> UserSecurityQuestions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=psqlbankapi.cxgpdkdtwgbh.ap-south-1.rds.amazonaws.com;Database=psqlbankapi;Username=postgres;Password=Welcome1!");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("pgcrypto");

            modelBuilder.Entity<AccountStatement>(entity =>
            {
                entity.HasKey(e => e.HashId)
                    .HasName("account_statements_pkey");

                entity.ToTable("account_statements");

                entity.Property(e => e.HashId)
                    .HasMaxLength(100)
                    .HasColumnName("hash_id");

                entity.Property(e => e.AllAttributes)
                    .HasColumnType("json")
                    .HasColumnName("all_attributes");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.CreditAmount).HasColumnName("credit_amount");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.DebitAmount).HasColumnName("debit_amount");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasColumnName("description");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.AccountStatements)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("account_statements_bank_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AccountStatements)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("account_statements_user_id_fkey");
            });

            modelBuilder.Entity<AccountSummery>(entity =>
            {
                entity.HasKey(e => new { e.BankId, e.UserId, e.AccountType, e.AccountNo })
                    .HasName("account_summery_pkey");

                entity.ToTable("account_summery");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.AccountType)
                    .HasMaxLength(20)
                    .HasColumnName("account_type");

                entity.Property(e => e.AccountNo)
                    .HasMaxLength(25)
                    .HasColumnName("account_no");

                entity.Property(e => e.AllAttributes)
                    .HasColumnType("json")
                    .HasColumnName("all_attributes");

                entity.Property(e => e.AutoClosure)
                    .HasMaxLength(10)
                    .HasColumnName("auto_closure");

                entity.Property(e => e.AutoRenewal)
                    .HasMaxLength(10)
                    .HasColumnName("auto_renewal");

                entity.Property(e => e.Balance).HasColumnName("balance");

                entity.Property(e => e.DateOfDeposit).HasColumnName("date_of_deposit");

                entity.Property(e => e.DepositAmount).HasColumnName("deposit_amount");

                entity.Property(e => e.DepositType)
                    .HasMaxLength(40)
                    .HasColumnName("deposit_type");

                entity.Property(e => e.InterestRate).HasColumnName("interest_rate");

                entity.Property(e => e.MaturityAmount).HasColumnName("maturity_amount");

                entity.Property(e => e.MaturityDate).HasColumnName("maturity_date");

                entity.Property(e => e.NominationRegistered)
                    .HasMaxLength(10)
                    .HasColumnName("nomination_registered");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.AccountSummeries)
                    .HasForeignKey(d => d.BankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("account_summery_bank_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AccountSummeries)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("account_summery_user_id_fkey");
            });

            modelBuilder.Entity<AccountTransaction>(entity =>
            {
                entity.HasKey(e => e.HashId)
                    .HasName("account_transactions_pkey");

                entity.ToTable("account_transactions");

                entity.Property(e => e.HashId)
                    .HasMaxLength(100)
                    .HasColumnName("hash_id");

                entity.Property(e => e.AllAttributes)
                    .HasColumnType("json")
                    .HasColumnName("all_attributes");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.ClosingBalance).HasColumnName("closing_balance");

                entity.Property(e => e.CreditAmount).HasColumnName("credit_amount");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.DebitAmount).HasColumnName("debit_amount");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasColumnName("description");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Reference)
                    .HasMaxLength(50)
                    .HasColumnName("reference");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.AccountTransactions)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("account_transactions_bank_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AccountTransactions)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("account_transactions_user_id_fkey");
            });

            modelBuilder.Entity<AccountTransactionsAnalysis>(entity =>
            {
                entity.HasKey(e => e.HashId)
                    .HasName("account_transactions_analysis_pkey");

                entity.ToTable("account_transactions_analysis");

                entity.Property(e => e.HashId)
                    .HasMaxLength(100)
                    .HasColumnName("hash_id");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.ClosingBalance).HasColumnName("closing_balance");

                entity.Property(e => e.CreditAmount).HasColumnName("credit_amount");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.DebitAmount).HasColumnName("debit_amount");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasColumnName("description");

                entity.Property(e => e.DescriptionId).HasColumnName("description_id");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Label)
                    .HasMaxLength(100)
                    .HasColumnName("label");

                entity.Property(e => e.Reference)
                    .HasMaxLength(50)
                    .HasColumnName("reference");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.AccountTransactionsAnalyses)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("account_transactions_analysis_bank_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AccountTransactionsAnalyses)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("account_transactions_analysis_user_id_fkey");
            });

            modelBuilder.Entity<Apilogger>(entity =>
            {
                entity.HasKey(e => e.Logid)
                    .HasName("apilogger_pkey");

                entity.ToTable("apilogger");

                entity.Property(e => e.Logid)
                    .HasColumnName("logid")
                    .UseIdentityAlwaysColumn();

                entity.Property(e => e.Codemessage)
                    .HasColumnType("character varying")
                    .HasColumnName("codemessage");

                entity.Property(e => e.Datetime)
                    .HasColumnName("datetime")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Innermessage)
                    .HasColumnType("character varying")
                    .HasColumnName("innermessage");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasColumnType("character varying")
                    .HasColumnName("message");

                entity.Property(e => e.Sourceoferror)
                    .HasColumnType("character varying")
                    .HasColumnName("sourceoferror");

                entity.Property(e => e.Statuscode)
                    .HasColumnType("character varying")
                    .HasColumnName("statuscode");
            });

            modelBuilder.Entity<BankXpath>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("bank_xpath");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.BankUrl)
                    .HasMaxLength(100)
                    .HasColumnName("bank_url");

                entity.Property(e => e.CaptchaXpath)
                    .HasMaxLength(100)
                    .HasColumnName("captcha_xpath");

                entity.Property(e => e.LoginXpath)
                    .HasMaxLength(100)
                    .HasColumnName("login_xpath");

                entity.Property(e => e.PasswordXpath)
                    .HasMaxLength(100)
                    .HasColumnName("password_xpath");

                entity.Property(e => e.UseridXpath)
                    .HasMaxLength(100)
                    .HasColumnName("userid_xpath");

                entity.HasOne(d => d.Bank)
                    .WithMany()
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("bank_xpath_bank_id_fkey");
            });

            modelBuilder.Entity<BanksInfo>(entity =>
            {
                entity.HasKey(e => e.BankId)
                    .HasName("banks_info_pkey");

                entity.ToTable("banks_info");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.BankName)
                    .HasMaxLength(50)
                    .HasColumnName("bank_name");

                entity.Property(e => e.OtpEnabled).HasColumnName("otp_enabled");
            });

            modelBuilder.Entity<CustomerAccountSummery>(entity =>
            {
                entity.HasKey(e => e.CustomerAccSummeryId)
                    .HasName("customer_account_summery_pkey");

                entity.ToTable("customer_account_summery");

                entity.Property(e => e.CustomerAccSummeryId).HasColumnName("customer_acc_summery_id");

                entity.Property(e => e.Customerbankid).HasColumnName("customerbankid");

                entity.Property(e => e.Depositbalance).HasColumnName("depositbalance");

                entity.Property(e => e.Savingbalance).HasColumnName("savingbalance");

                entity.HasOne(d => d.Customerbank)
                    .WithMany(p => p.CustomerAccountSummeries)
                    .HasForeignKey(d => d.Customerbankid)
                    .HasConstraintName("customerbankid");
            });

            modelBuilder.Entity<CustomerAccountTransaction>(entity =>
            {
                entity.HasKey(e => e.CustomerTransactionId)
                    .HasName("customer_account_transaction_pkey");

                entity.ToTable("customer_account_transaction");

                entity.Property(e => e.CustomerTransactionId)
                    .HasColumnName("customer_transaction_id")
                    .HasDefaultValueSql("nextval('foo_a_seq'::regclass)");

                entity.Property(e => e.ClosingBalance)
                    .HasColumnType("character varying")
                    .HasColumnName("Closing_Balance");

                entity.Property(e => e.Createddate)
                    .HasColumnName("createddate")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Customerbankid).HasColumnName("customerbankid");

                entity.Property(e => e.Date).HasColumnType("date");

                entity.Property(e => e.Deposit).HasColumnType("character varying");

                entity.Property(e => e.Narration).HasColumnType("character varying");

                entity.Property(e => e.Refno)
                    .HasColumnType("character varying")
                    .HasColumnName("refno");

                entity.Property(e => e.ValueDate)
                    .HasColumnType("date")
                    .HasColumnName("Value_Date");

                entity.Property(e => e.Withdrawal).HasColumnType("character varying");
            });

            modelBuilder.Entity<CustomerAccountTransactionAnalysis>(entity =>
            {
                entity.HasKey(e => e.CustomerTransactionId)
                    .HasName("customer_account_transaction_analysis_pkey");

                entity.ToTable("customer_account_transaction_analysis");

                entity.Property(e => e.CustomerTransactionId)
                    .ValueGeneratedNever()
                    .HasColumnName("customer_transaction_id");

                entity.Property(e => e.ClosingBalance)
                    .HasMaxLength(25)
                    .HasColumnName("closing_balance");

                entity.Property(e => e.Customerbankid).HasColumnName("customerbankid");

                entity.Property(e => e.Date)
                    .HasColumnType("date")
                    .HasColumnName("date");

                entity.Property(e => e.Deposit)
                    .HasMaxLength(25)
                    .HasColumnName("deposit");

                entity.Property(e => e.Label)
                    .HasMaxLength(50)
                    .HasColumnName("label");

                entity.Property(e => e.Narration)
                    .HasMaxLength(300)
                    .HasColumnName("narration");

                entity.Property(e => e.Refno)
                    .HasMaxLength(50)
                    .HasColumnName("refno");

                entity.Property(e => e.ValueDate)
                    .HasColumnType("date")
                    .HasColumnName("value_date");

                entity.Property(e => e.Withdrawal)
                    .HasMaxLength(25)
                    .HasColumnName("withdrawal");

                entity.HasOne(d => d.Customerbank)
                    .WithMany(p => p.CustomerAccountTransactionAnalyses)
                    .HasForeignKey(d => d.Customerbankid)
                    .HasConstraintName("customer_account_transaction_analysis_customerbankid_fkey");
            });

            modelBuilder.Entity<CustomerBankInfo>(entity =>
            {
                entity.HasKey(e => e.Customerbankid)
                    .HasName("customer_bank_info_pkey");

                entity.ToTable("customer_bank_info");

                entity.Property(e => e.Customerbankid).HasColumnName("customerbankid");

                entity.Property(e => e.AccountNo)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("account_no");

                entity.Property(e => e.AccountType)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("account_type");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.Branch)
                    .HasColumnType("character varying")
                    .HasColumnName("branch");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.Ifsc)
                    .HasColumnType("character varying")
                    .HasColumnName("ifsc");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(20)
                    .HasColumnName("mobile");

                entity.Property(e => e.Name)
                    .HasColumnType("character varying")
                    .HasColumnName("name");

                entity.Property(e => e.Otp).HasColumnName("otp");

                entity.Property(e => e.Password)
                    .HasMaxLength(200)
                    .HasColumnName("password");

                entity.Property(e => e.Username)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("username");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.CustomerBankInfos)
                    .HasForeignKey(d => d.BankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_bank_info_bank_id_fkey");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerBankInfos)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_id");
            });

            modelBuilder.Entity<CustomerBankSecured>(entity =>
            {
                entity.HasKey(e => e.Customerbankid)
                    .HasName("customer_bank_secured_pkey");

                entity.ToTable("customer_bank_secured");

                entity.Property(e => e.Customerbankid)
                    .ValueGeneratedNever()
                    .HasColumnName("customerbankid");

                entity.Property(e => e.AccountNo)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("account_no");

                entity.Property(e => e.AccountType)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("account_type");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.Branch)
                    .HasColumnType("character varying")
                    .HasColumnName("branch");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.Ifsc)
                    .HasColumnType("character varying")
                    .HasColumnName("ifsc");

                entity.Property(e => e.Logindata)
                    .IsRequired()
                    .HasColumnName("logindata");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(20)
                    .HasColumnName("mobile");

                entity.Property(e => e.Name)
                    .HasColumnType("character varying")
                    .HasColumnName("name");

                entity.Property(e => e.Otp).HasColumnName("otp");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.CustomerBankSecureds)
                    .HasForeignKey(d => d.BankId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customer_bank_secured_bank_id_fkey");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.CustomerBankSecureds)
                    .HasForeignKey(d => d.CustomerId)
                    .HasConstraintName("customer_id");
            });

            modelBuilder.Entity<CustomerProfile>(entity =>
            {
                entity.HasKey(e => e.Customerid)
                    .HasName("customer_profile_pkey");

                entity.ToTable("customer_profile");

                entity.Property(e => e.Customerid).HasColumnName("customerid");

                entity.Property(e => e.Address).HasColumnName("address");

                entity.Property(e => e.Age).HasColumnName("age");

                entity.Property(e => e.Apikey).HasColumnName("apikey");

                entity.Property(e => e.Email).HasColumnName("email");

                entity.Property(e => e.Expireson).HasColumnName("expireson");

                entity.Property(e => e.Isverified).HasColumnName("isverified");

                entity.Property(e => e.Managementid).HasColumnName("managementid");

                entity.Property(e => e.MaritalStatus).HasColumnName("marital_status");

                entity.Property(e => e.Mobile).HasColumnName("mobile");

                entity.Property(e => e.Name).HasColumnName("name");

                entity.Property(e => e.Otp).HasColumnName("otp");
            });

            modelBuilder.Entity<DepositStatement>(entity =>
            {
                entity.HasKey(e => e.HashId)
                    .HasName("deposit_statements_pkey");

                entity.ToTable("deposit_statements");

                entity.Property(e => e.HashId)
                    .HasMaxLength(100)
                    .HasColumnName("hash_id");

                entity.Property(e => e.AllAttributes)
                    .HasColumnType("json")
                    .HasColumnName("all_attributes");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.CreditAmount).HasColumnName("credit_amount");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.DebitAmount).HasColumnName("debit_amount");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasColumnName("description");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.DepositStatements)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("deposit_statements_bank_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DepositStatements)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("deposit_statements_user_id_fkey");
            });

            modelBuilder.Entity<DepositTransaction>(entity =>
            {
                entity.HasKey(e => e.HashId)
                    .HasName("deposit_transactions_pkey");

                entity.ToTable("deposit_transactions");

                entity.Property(e => e.HashId)
                    .HasMaxLength(100)
                    .HasColumnName("hash_id");

                entity.Property(e => e.AllAttributes)
                    .HasColumnType("json")
                    .HasColumnName("all_attributes");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.ClosingBalance).HasColumnName("closing_balance");

                entity.Property(e => e.CreditAmount).HasColumnName("credit_amount");

                entity.Property(e => e.Date).HasColumnName("date");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasColumnName("description");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Bank)
                    .WithMany(p => p.DepositTransactions)
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("deposit_transactions_bank_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.DepositTransactions)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("deposit_transactions_user_id_fkey");
            });

            modelBuilder.Entity<UserProfile>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("user_profile_pkey");

                entity.ToTable("user_profile");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address");

                entity.Property(e => e.Age).HasColumnName("age");

                entity.Property(e => e.Email)
                    .HasMaxLength(30)
                    .HasColumnName("email");

                entity.Property(e => e.MaritalStatus)
                    .HasMaxLength(10)
                    .HasColumnName("marital_status");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(20)
                    .HasColumnName("mobile");

                entity.Property(e => e.Name)
                    .HasMaxLength(40)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<UserSecurityQuestion>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("user_security_questions");

                entity.Property(e => e.Answer)
                    .HasMaxLength(50)
                    .HasColumnName("answer");

                entity.Property(e => e.BankId).HasColumnName("bank_id");

                entity.Property(e => e.Id)
                    .ValueGeneratedOnAdd()
                    .HasColumnName("id");

                entity.Property(e => e.Question)
                    .HasMaxLength(100)
                    .HasColumnName("question");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Bank)
                    .WithMany()
                    .HasForeignKey(d => d.BankId)
                    .HasConstraintName("user_security_questions_bank_id_fkey");

                entity.HasOne(d => d.User)
                    .WithMany()
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("user_security_questions_user_id_fkey");
            });

            modelBuilder.HasSequence("foo_a_seq");

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
