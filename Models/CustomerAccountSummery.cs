﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class CustomerAccountSummery
    {
        public decimal? Savingbalance { get; set; }
        public decimal? Depositbalance { get; set; }
        public int? Customerbankid { get; set; }
        public int CustomerAccSummeryId { get; set; }

        public virtual CustomerBankInfo Customerbank { get; set; }
    }
}
