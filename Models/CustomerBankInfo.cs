﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class CustomerBankInfo
    {
        public CustomerBankInfo()
        {
            CustomerAccountSummeries = new HashSet<CustomerAccountSummery>();
            CustomerAccountTransactionAnalyses = new HashSet<CustomerAccountTransactionAnalysis>();
        }

        public int BankId { get; set; }
        public string Mobile { get; set; }
        public string AccountType { get; set; }
        public string AccountNo { get; set; }
        public string Password { get; set; }
        public long? CustomerId { get; set; }
        public string Username { get; set; }
        public int Customerbankid { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        public string Ifsc { get; set; }
        public int? Otp { get; set; }

        public virtual BanksInfo Bank { get; set; }
        public virtual CustomerProfile Customer { get; set; }
        public virtual ICollection<CustomerAccountSummery> CustomerAccountSummeries { get; set; }
        public virtual ICollection<CustomerAccountTransactionAnalysis> CustomerAccountTransactionAnalyses { get; set; }
    }
}
