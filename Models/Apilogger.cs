﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class Apilogger
    {
        public long Logid { get; set; }
        public string Message { get; set; }
        public string Innermessage { get; set; }
        public DateTime Datetime { get; set; }
        public string Statuscode { get; set; }
        public string Codemessage { get; set; }
        public string Sourceoferror { get; set; }
    }
}
