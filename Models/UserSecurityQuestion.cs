﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class UserSecurityQuestion
    {
        public int Id { get; set; }
        public int? BankId { get; set; }
        public int? UserId { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }

        public virtual BanksInfo Bank { get; set; }
        public virtual UserProfile User { get; set; }
    }
}
