﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class BankXpath
    {
        public int? BankId { get; set; }
        public string BankUrl { get; set; }
        public string UseridXpath { get; set; }
        public string PasswordXpath { get; set; }
        public string CaptchaXpath { get; set; }
        public string LoginXpath { get; set; }

        public virtual BanksInfo Bank { get; set; }
    }
}
