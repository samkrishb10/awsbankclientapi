﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace AWSBankClientAPI.Models
{
    //General Class Properties
    #region
    public class MailValidation
    {
        public string email { get; set; }
    }
    public class VerifyOTPEmail
    {
        public string email { get; set; }
        public int otp { get; set; }
    }
    public class GenerateToken
    {
        public string email { get; set; }
        public string apikey { get; set; }
        //public string secretkey { get; set; }
    }

    public class BankAccount
    {
        public string username { get; set; }
        public string password { get; set; }
        public int customerid { get; set; }
        public int bankid { get; set; }
    }
    public class UserAccount
    {
        public long customerid { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string token { get; set; }
    }
    public class UpdateOtp
    {
        public int customerid { get; set; }
        public int bankid { get; set; }
        public string username { get; set; }
        public int otp { get; set; }
    }
    public enum BankNames
    {
        HDFC_Bank = 1,
        State_Bank_of_India = 2,
        Andhra_Bank = 3,
        Axis_Bank = 4,
        ICICI_Personal_Bank = 5,
        ICICI_Corporate_Bank = 6,
        Punjab_National_Bank = 7,
        Union_Bank_Of_Philippines = 8,
        LIC_Housing_Finance = 9
    }
    public static class BankAPIS
    {
        public static string GetBankURLS(int bnid)
        {
            string urls;
            switch ((BankNames)bnid)
            {
                case BankNames.HDFC_Bank:
                    urls = "api/hdfcbanking";
                    break;
                case BankNames.State_Bank_of_India:
                    urls = "api/sbibanking";
                    break;
                case BankNames.Andhra_Bank:
                    urls = "api/andhrabanking";
                    break;
                case BankNames.Axis_Bank:
                    urls = "api/axisbanking";
                    break;
                case BankNames.ICICI_Personal_Bank:
                    urls = "api/icicbanking";
                    break;
                case BankNames.ICICI_Corporate_Bank:
                    urls = "api/iciccorporatescraper";
                    break;
                case BankNames.Punjab_National_Bank:
                    urls = "api/pnbbanking";
                    break;
                case BankNames.Union_Bank_Of_Philippines:
                    urls = "";
                    break;
                case BankNames.LIC_Housing_Finance:
                    urls = "api/lichfinloan";
                    break;
                default:
                    urls = "api/hdfcbanking";
                    break;
            }
            return urls;
        }
    }
    #endregion

    //User Summary Request
    #region
    public class UserSummaryRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string data_type { get; set; }
    }
    public class SBIUserSummaryRequest
    {
        public long customer_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string data_type { get; set; }
    }
    //public class UBPSummaryRequest
    //{
    //    public string username { get; set; }
    //    public string password { get; set; }
    //    public string account_name { get; set; }
    //}
    public class UserTransactionRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string data_type { get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
    }
    public class UserSummeryRequestsParam
    {
        public List<UserSummaryRequest> users { get; set; }
    }
    public class SBIUserSummeryRequestsParam
    {
        public List<SBIUserSummaryRequest> users { get; set; }
    }
    public class UserTransactionRequestsParam
    {
        public List<UserTransactionRequest> users { get; set; }
    }
    #endregion

    //User Summary Response
    #region
    public class UserSummaryResponse
    {
        public int customer_acc_summery_id { get; set; }
        public long customerid { get; set; }
        public int bankid { get; set; }
        public decimal savingbalance { get; set; }
        public decimal depositbalance { get; set; }
        public string account_number { get; set; }
        public string branch_address { get; set; }
        public string ifsc_code { get; set; }
        public string name { get; set; }
    }
    public class ICICIUserSummaryResponse
    {
        public int index { get; set; }
        public string Account_Number { get; set; }
        public string Branch_Address { get; set; }
        public string IFSC_Code { get; set; }
        public string Currency { get; set; }
        public string All_Bank_Accounts { get; set; }
        public string All_Deposit_Accounts { get; set; }
        public string All_Demat_Accounts { get; set; }
        public string All_Loan_Accounts { get; set; }
        public string All_Credit_Card_Accounts { get; set; }
    }
    public class HDFCUserSummaryResponse
    {
        public int index { get; set; }
        [JsonProperty("Account_No.")]
        public string Account_No { get; set; }
        public string Branch { get; set; }
        public string Name { get; set; }
        public string Available_Balance { get; set; }
    }
    public class ICICICORPUserSummaryResponse
    {
        public int index { get; set; }
        public string ACCOUNT_CURRENCY { get; set; }
        public string CUSTOMER_ID { get; set; }
        public string LEDGER_BALANCE { get; set; }
        public string EFFECTIVE_AVAILABLE_BALANCE { get; set; }
        public string Account_Number { get; set; }
    }
    public class PNBUserSummaryResponse
    {
        public int index { get; set; }
        public string Account_Name { get; set; }
        public string Branch { get; set; }
        public string Available_Balance { get; set; }
        public string Effective_Available_Balance { get; set; }
        [JsonProperty("Account Number")]
        public string Account_Number { get; set; }
    }
    public class ABUserSummaryResponse
    {
        public int index { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        [JsonProperty("Available Balance")]
        public string Available_Balance { get; set; }
        [JsonProperty("Effective Available Balance")]
        public string Effective_Available_Balance { get; set; }
        public string Number { get; set; }
    }
    public class AxisUserSummaryResponse
    {
        public int index { get; set; }
        public string Name { get; set; }
        public string Branch { get; set; }
        [JsonProperty("Available Balance")]
        public string Available_Balance { get; set; }
        [JsonProperty("Effective Available Balance")]
        public string Effective_Available_Balance { get; set; }
        [JsonProperty("Account No.")]
        public string Number { get; set; }
    }
    public class LICHFINUserSummaryResponse
    {
        public int index { get; set; }
        public string Name { get; set; }
        [JsonProperty("Area Office")]
        public string Branch { get; set; }
        [JsonProperty("Disbursement Amount")]
        public string Available_Balance { get; set; }
        [JsonProperty("Loan Number")]
        public string Number { get; set; }
    }

    public class SBIUserSummaryResponse
    {
        public int index { get; set; }
        public string Branch { get; set; }
        public string Available_Balance { get; set; }
        [JsonProperty("Account_No._/_Name")]
        public string Account_No { get; set; }
    }
    #endregion

    //User Transaction Response
    #region
    public class UserTransactResponse
    {
        public int customer_transaction_id { get; set; }
        public long customerid { get; set; }
        public int bankid { get; set; }
        public string Date { get; set; }
        public string Narration { get; set; }
        public string Refno { get; set; }
        public string Valuedate { get; set; }
        public string Withdrawal { get; set; }
        public string Deposit { get; set; }
        public string Closing_Balance { get; set; }
    }
    public class HDFCUserTransactResponse
    {
        public int index { get; set; }
        public string Date { get; set; }
        public string Value_Date { get; set; }
        public string Narration { get; set; }
        public string Withdrawal { get; set; }
        public string Deposit { get; set; }
        public string Closing_Balance { get; set; }

        [JsonProperty("Cheque/Ref._No.")]
        public string RefNo { get; set; }
    }
    public class ICICIUserTransactResponse
    {
        public int index { get; set; }
        [JsonProperty("S_No.")]
        public string S_No { get; set; }
        public string Value_Date { get; set; }
        public string Transaction_Date { get; set; }
        public string Cheque_Number { get; set; }
        public string Transaction_Remarks { get; set; }
        [JsonProperty("Withdrawal_Amount_(INR)")]
        public string Withdrawal_Amount { get; set; }
        [JsonProperty("Deposit_Amount_(INR)")]
        public string Deposit_Amount { get; set; }
        [JsonProperty("Balance_(INR)")]
        public string Balance { get; set; }
    }
    public class ICICICORPUserTransactResponse
    {
        public int index { get; set; }
        public string Transaction_Date { get; set; }
        public string Value_Date { get; set; }
        public string Instruments_ID { get; set; }
        public string Description { get; set; }
        [JsonProperty("Debit_Amount_(INR)")]
        public string Debit_Amount { get; set; }
        [JsonProperty("Credit_Amount_(INR)")]
        public string Credit_Amount { get; set; }
        [JsonProperty("Account_Balance_(INR)")]
        public string Account_Balance { get; set; }
    }

    public class PNBUserTransactResponse
    {
        public int index { get; set; }
        public string Select { get; set; }
        public string Transaction_Date { get; set; }
        public string Instrument_ID { get; set; }
        public string Transaction_Remarks { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }
        public string Account_Balance { get; set; }
    }
    public class ABUserTransactResponse
    {
        public int index { get; set; }
        [JsonProperty("Tran Id")]
        public string TransactionID { get; set; }
        public string Date { get; set; }
        public string Remarks { get; set; }
        [JsonProperty("Instrument ID")]
        public string Instrument_ID { get; set; }
        [JsonProperty("Amount (INR)")]
        public string Amount { get; set; }
        [JsonProperty("Balance (INR)")]
        public string Account_Balance { get; set; }
        public string Transaction { get; set; }
    }
    public class LICHFINUserTransactResponse
    {
        public int index { get; set; }
        [JsonProperty("Tran Id")]
        public string TransactionID { get; set; }
        public string Date { get; set; }
        public string Remarks { get; set; }
        [JsonProperty("Instrument ID")]
        public string Instrument_ID { get; set; }
        [JsonProperty("Amount (INR)")]
        public string Amount { get; set; }
        [JsonProperty("Balance (INR)")]
        public string Account_Balance { get; set; }
        public string Transaction { get; set; }
    }

    public class AxisUserTransactResponse
    {
        public int index { get; set; }
        [JsonProperty("Tran Date")]
        public string Date { get; set; }
        [JsonProperty("Value Date")]
        public string Value_Date { get; set; }
        [JsonProperty("Cheque No.")]
        public string Instrument_ID { get; set; }
        [JsonProperty("Amount")]
        public string Amount { get; set; }
        [JsonProperty("Balance")]
        public string Balance { get; set; }
        [JsonProperty("Transaction Details")]
        public string Remarks { get; set; }
        [JsonProperty("DR/CR")]
        public string Transaction { get; set; }
    }
    #endregion

    //Inner Class
    #region
    [Serializable]
    public class LoginCred
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class Secureddata
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int CustomerBankId { get; set; }
        public int BankId { get; set; }
    }
    public class CommonMethods
    {
        public static byte[] ConvertObjToBytes(object cred)
        {
            byte[] bytes;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, cred);
                bytes = ms.ToArray();
            }

            return bytes;
        }
        public static object DeserializeFromStream(MemoryStream stream)
        {
            IFormatter formatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            object o = formatter.Deserialize(stream);
            return o;
        }
    }
    #endregion

    //public class UBP_BalanceResp
    //{
    //    public string type { get; set; }
    //    public int amount { get; set; }
    //    public string currency { get; set; }
    //}
    //public class UBP_AccountCreateResponse
    //{
    //    public string msg { get; set; }
    //    public int code { get; set; }
    //    public int status { get; set; }
    //    public data data { get; set; }
    //}
    //public class data
    //{
    //    public user user { get; set; }
    //    public account account { get; set; }
    //}
    //public class user
    //{
    //    public string username { get; set; }
    //    public string password { get; set; }
    //}
    //public class account
    //{
    //    public string user_id { get; set; }
    //    public string account_number { get; set; }
    //    public string card_number { get; set; }
    //    public string account_name { get; set; }
    //    public string account_code { get; set; }
    //    public string account_type { get; set; }
    //    public string status { get; set; }
    //    public string branch { get; set; }
    //    public string balance { get; set; }
    //}

}
