﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWSBankClientAPI.Models
{
    public class AppSettings
    {
        public string bankapiurl { get; set; }
        public string Key { get; set; }
        public string Admin_Mail { get; set; }
        public string Admin_Password { get; set; }
        public string Admin_Name { get; set; }
        public string SMTP_Server { get; set; }
        public string Site_Name { get; set; }
        public int SMTP_Port { get; set; }
        public string UBP_url { get; set; }
        public string UBP_client_id { get; set; }
        public string UBP_client_secret { get; set; }
        public string UBP_username { get; set; }
        public string UBP_password { get; set; }
        public string MasterKey { get; set; }

    }
}
