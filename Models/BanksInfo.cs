﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class BanksInfo
    {
        public BanksInfo()
        {
            AccountStatements = new HashSet<AccountStatement>();
            AccountSummeries = new HashSet<AccountSummery>();
            AccountTransactions = new HashSet<AccountTransaction>();
            AccountTransactionsAnalyses = new HashSet<AccountTransactionsAnalysis>();
            CustomerBankInfos = new HashSet<CustomerBankInfo>();
            CustomerBankSecureds = new HashSet<CustomerBankSecured>();
            DepositStatements = new HashSet<DepositStatement>();
            DepositTransactions = new HashSet<DepositTransaction>();
        }

        public int BankId { get; set; }
        public string BankName { get; set; }
        public bool? OtpEnabled { get; set; }

        public virtual ICollection<AccountStatement> AccountStatements { get; set; }
        public virtual ICollection<AccountSummery> AccountSummeries { get; set; }
        public virtual ICollection<AccountTransaction> AccountTransactions { get; set; }
        public virtual ICollection<AccountTransactionsAnalysis> AccountTransactionsAnalyses { get; set; }
        public virtual ICollection<CustomerBankInfo> CustomerBankInfos { get; set; }
        public virtual ICollection<CustomerBankSecured> CustomerBankSecureds { get; set; }
        public virtual ICollection<DepositStatement> DepositStatements { get; set; }
        public virtual ICollection<DepositTransaction> DepositTransactions { get; set; }
    }
}
