﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class CustomerAccountTransaction
    {
        public int CustomerTransactionId { get; set; }
        public DateTime? Date { get; set; }
        public string Narration { get; set; }
        public string Refno { get; set; }
        public DateTime? ValueDate { get; set; }
        public string Withdrawal { get; set; }
        public string Deposit { get; set; }
        public string ClosingBalance { get; set; }
        public int? Customerbankid { get; set; }
        public DateTime Createddate { get; set; }
    }
}
