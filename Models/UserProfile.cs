﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AWSBankClientAPI.Models
{
    public partial class UserProfile
    {
        public UserProfile()
        {
            AccountStatements = new HashSet<AccountStatement>();
            AccountSummeries = new HashSet<AccountSummery>();
            AccountTransactions = new HashSet<AccountTransaction>();
            AccountTransactionsAnalyses = new HashSet<AccountTransactionsAnalysis>();
            DepositStatements = new HashSet<DepositStatement>();
            DepositTransactions = new HashSet<DepositTransaction>();
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public int? Age { get; set; }
        public string MaritalStatus { get; set; }
        public string Address { get; set; }

        public virtual ICollection<AccountStatement> AccountStatements { get; set; }
        public virtual ICollection<AccountSummery> AccountSummeries { get; set; }
        public virtual ICollection<AccountTransaction> AccountTransactions { get; set; }
        public virtual ICollection<AccountTransactionsAnalysis> AccountTransactionsAnalyses { get; set; }
        public virtual ICollection<DepositStatement> DepositStatements { get; set; }
        public virtual ICollection<DepositTransaction> DepositTransactions { get; set; }
    }
}
