﻿using System;
using System.IO;

/// <summary>
/// Summary description for Common
/// </summary>
public class Common
{
    public Common()
    {
    }
    public static string WriteLogFile = "Log.txt";
    public static void WriteLog(string context)
    {
        File.AppendAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory + WriteLogFile), DateTime.Now + System.Environment.NewLine + context + System.Environment.NewLine + "--------------------------------------------------------------------" + System.Environment.NewLine);
    }
}